$(document).ready(function() {
  $('#q').on('keyup', function() {
    var qValue = $(this).val().trim(); // trim() retire les espaces et tabulation autour de la valeur

    if (qValue !== '') {
      $.ajax({
        url: './xhr/recherche_mots.php',
        method: 'GET',
        data: {
          q: qValue
        },
        dataType: 'json',
        success: function(data, textStatus, jqXHR) {
          console.log(data);
          if (data.length) {
            var suggestions = '<ul>';
            for (var i = 0; data[i]; i++) {
              suggestions += '<li>' + data[i] + '</li>';
            }
            suggestions += '</ul>';
            $('#suggestions').html(suggestions);
          } else {
            $('#suggestions').html('<p><em>Aucun résultat<em></p>');
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          console.error(jqXHR, textStatus, errorThrown);
        }
      });
    } else {
      $('#suggestions').html('');
    }
  });
});
