$(document).ready(function(){
    $('#connexion_valider').on('submit', function(event){
        event.preventDefault();
        //        console.log($(this).serialize());
        //        console.log('tdst');
        //        connexion('traitement_connexion.php', $(this).serialize())
        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            method: $(this).attr('method'),
            dataType: 'json',
            //renvoie la reponse du json_encode
            success: function(data) {
                console.log(data.validation);
                if (!data.validation) {
                    //le .empty vide le contenu de la div avant d'insérer de nouvelles infos
                    $('#result-connexion').empty().html(data.message);
                } else {
                    window.setTimeout(function () {location.href= data.redirect_url}, 2000);
                    $('#connexion').addClass('hidden');
                    $('#result-connexion').empty().html(data.message);
                }
            }
        });

    });


    $('#inscription_valider').on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            method: $(this).attr('method'),
            dataType: 'json',
            //renvoie la reponse du json_encode
            success: function(data) {
                console.log(data.data.validation);
                if (!data.data.validation) {
                    //le .empty vide le contenu de la div avant d'insérer de nouvelles infos
                    $('#result-inscription').empty().html(data.message);
                    //permet de boucler sur chaque erreur dans un tableau
                    //value est un tableau d'où le value.message
                    $('.error-input').empty();
                    $.each(data.errors, function( index, value ) {
                        console.log( index + ": " + value );
                        //permet de récupérer la classe error associée à l'index
                        $('.error-'+index).empty().html(value.message);
                    });
                } else {
                    window.setTimeout(function () {location.href= data.data.redirect_url}, 2000);
                    $('#inscription').addClass('hidden');
                    $('#result-inscription').empty().html(data.data.message);
                }
            }
        });

    });


});