<?php

//Connexion BDD
$host_name = '192.168.';
$database = 'troc';
$user_name = '';
$password = '';

$pdo = null;
try {
  $pdo = new PDO("mysql:host=$host_name; dbname=$database;", $user_name, $password,   array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
} catch (PDOException $e) {
  echo "Erreur!: " . $e->getMessage() . "<br/>";
  die();
}

//-------------- SESSION
session_start();

//-------------- CHEMIN
define("RACINE_SITE", $_SERVER['DOCUMENT_ROOT'] . "/Troc2/");
//echo '<pre>'; print_r($_SERVER); echo '</pre>';
//echo RACINE_SITE;
/*
cette constante retourne le chemin physique du dossier boutique sur le serveur
Lors de l'enregistrement d'images/photos, nous aurons besoin du chemin complet du dossier photo pour enregistrer la photo 
*/

define("URL", 'http://localhost/Troc2/');
// Cette constante servira à enregister l'URL d'une photo/image dans la BDD, on ne conserve jamais la photo elle même, ce serait trop lourd pour la BDD

//------------- VARIABLE
$content = '';

//------------- INCLUSIONS
require_once("fonctions.inc.php");
?>