<?php

function debug($param) {
	echo '<div style="border: 1px solid red; margin: 0 100px;">';
			echo '<pre>'; print_r($param); echo '</pre>';
	echo '</div>';
}


//--------------------------
// Fonctions liées au membre :

function internauteEstConnecte() {
	// cette fonction indique si le membre est connecté :
	
	if (isset($_SESSION['membre'])) {
		// si l'indice "membre" existe dans la SESSION, c'est que l'internaute est passé par la page de connexion avec le bon mdp
		return true;
	} else {
		return false;
	}
	//-----
	// Ou alors on peut écrire :
	// return (isset($_SESSION['membre']));
}

function internauteEstConnecteEtEstAdmin() {
	if (internauteEstConnecte() && $_SESSION['membre']['statut'] == 1) {
		//si la session du membre est définie, nous pouvons regarder si son statut vaut 1 (= admin). Dans ce cas on retourne true :
		return true;
	}  else {
		return false;
	}
}


//--------------------------------
// fonction pour exécuter des requêtes :
function executeRequete($req, $param = array()) {
	// $req va recevoir la requête SQL sous forme de string, et $param les marqueurs associés aux valeurs sous forme d'array. $param est optionnel : sa valeur par défaut est un array vide
	
	// Si $param n'est pas vide, on fait un htmlspecialchars dessus :
	if (!empty($param)) {
		// on échappe les caractères spéciaux (<, >, "", '', &) que l'on convertit en entité HTML :
		foreach($param as $indice => $valeur) {
			$param[$indice] = htmlspecialchars($valeur, ENT_QUOTES);  // on prend la valeur de $param que l'on traite dans le htmlspecialchars et que l'on range au même indice $param[$indice]. Evite les injections JS et CSS.
			
			// $param[$indice] = htmlspecialchars($param[$indice], ENT_QUOTES);	
		}
	}
	
	global $pdo; // pour avoir accès à la variable $pdo globale qui représente la connexion à la BDD (elle est définit dans init.inc.php)
		
	$resultat = $pdo->prepare($req);
	$resultat->execute($param);
	return $resultat; // on retourne à la fin un objet PDOStatement à l'endroit où la fonction est appelée	
}


//-------------------------------------
function montantTotal() {
	$total = 0;
	for ($i = 0; $i < count($_SESSION['panier']['id_produit']); $i++) {
		
		$total += $_SESSION['panier']['quantite'][$i] * $_SESSION['panier']['prix'][$i];  // on multiplie la quantite par le prix de l'article d'indice $i, puis on ajoute le résultat au contenu déjà existant de $total avec l'opérateur +=		
	}

	return $total;	
	
}










