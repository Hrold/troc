<?php
if(internauteEstConnecte()){
	$id_membre_session = $_SESSION['membre']['id_membre'];
	
$resultat = $pdo->prepare("SELECT pseudo FROM membre WHERE id_membre = $id_membre_session ");
$resultat->execute();
$membre = $resultat->fetch(PDO::FETCH_ASSOC);
}


?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Ma boutique!!</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link rel="stylesheet" href="<?= URL ?>inc/css/style.css">  

        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> 

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>  
		

        <script src="<?= URL ?>js/main.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

	<script type="text/javascript" src="html5lightbox/jquery.js"></script>
	<script type="text/javascript" src="html5lightbox/html5lightbox.js"></script>
  </head>

  <body>

    <nav class="navbar navbar-inverse ma-nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <?php echo '<a class="navbar-brand" href="'.URL.'index.php">TROC</a>' ?>
        </div>
        <div id="navbar" class="collapse navbar-collapse text-center">
          <ul class="nav navbar-nav text-center ">
            <?php
            if(internauteEstConnecteEtEstAdmin())
            { // accés 
		
				echo'<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion <span class="caret"></span></a>
          <ul class="dropdown-menu">';
				echo '<li><a href="' . URL . 'admin/gestion_annonce.php?action=affichage">Gestion annonces</a></li>';
                

                echo '<li><a href="' . URL . 'admin/gestion_membre.php">Gestion membre</a></li>';
                
                echo '<li><a href="' . URL . 'admin/gestion_categorie.php?action=affichage">Gestion catégories</a></li>';
				echo '<li><a href="' . URL . 'admin/gestion_commentaire.php?action=affichage">Gestion commmentaires</a></li>';
				echo '<li><a href="' . URL . 'admin/gestion_note.php?action=affichage">Gestion notes</a></li>';
				echo '</ul></li>';
            }
            if(internauteEstConnecte())
            { // accés membre
				echo '<li><a href="'.URL.'nous.php">Qui sommes-nous ?</li>';
				echo '<li><a href="'.URL.'contact.php">Contact</li>';
                echo '<li><a href="' . URL . 'profil.php">Profil</a></li>';
                echo '<li><a href="' . URL . 'user/ajout_annonce.php?action=affichage">Vos annonces</a></li>';
                echo '<li><a href="' . URL . 'connexion.php?action=deconnexion">Deconnexion</a></li>';
				echo '<li class="alert alert-success text-center">Bonjour<strong class="text-success"> ' . $membre['pseudo'] . '</strong></li>';
            }
            else
            { // visiteur
				echo '<li><a href="'.URL.'nous.php">Qui sommes-nous ?</li>';
				echo '<li><a href="'.URL.'contact.php">Contact</li>';
                echo '<li><a href="' . URL . 'inscription.php">Inscription</a></li>';
                echo '<li><a href="' . URL . 'connexion.php">Connexion</a></li>';
				echo '<li class="alert text-center text-danger">Veuillez cliquez sur <strong>Connexion</strong> pour vous connecter</li>';
            }
            ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container mon-conteneur">
        
        
        
        
        