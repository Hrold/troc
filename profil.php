<?php
require_once("inc/init.inc.php");
require_once("inc/fonctions.inc.php");
if(!internauteEstConnecte()) // si l'internaute n'est pas connecté, il n'a rien à faire sur la page profil, on le redirige vers la page connexion
{
    header("location:connexion.php");
}
$id_membre_session = $_SESSION['membre']['id_membre'];
$pseudo_membre_session = $_SESSION['membre']['pseudo'];
$pseudo_membre_email = $_SESSION['membre']['email'];

if(!empty($_POST))
    {
		
		 $erreur = '';
    
     $verif_pseudo = $pdo->prepare("SELECT * FROM membre WHERE pseudo = :pseudo");
    $verif_pseudo->bindValue(':pseudo', $_POST['pseudo']);
    $resultat = $verif_pseudo->execute();
    if($verif_pseudo->rowCount() > 1 && ($_POST['pseudo']!==$pseudo_membre_session))
    {
        $erreur .= '<div class="alert alert-danger col-md-offset-2 text-center">Pseudo indisponible!!</div>';
    }
    //------------------------------------------------------
    $verif_email = $pdo->prepare("SELECT * FROM membre WHERE email = :email");
     $verif_email->bindValue(':email', $_POST['email']);
    $verif_email->execute();
    if($verif_email->rowCount() > 1 &&($_POST['email']!==$pseudo_membre_email) )
     {
          $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Cet email existe dèjà dans notre base !</div>';
     }
    //------------------------------------------------------
     if(strlen($_POST['pseudo']) < 4 || strlen($_POST['pseudo']) > 20)
     {
          $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille de pseudo non valide, il doit contenir entre 4 et 20 caractères!</div>';
     }
    //------------------------------------------------------
    /*  if(strlen($_POST['mdp']) < 4 || strlen($_POST['mdp']) > 20)
     {
          $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille de mot de passe non valide, doit contenir entre 4 et 20 caractères!</div>';
    } */
    //------------------------------------------------------
    if(!is_numeric($_POST['telephone']) || strlen($_POST['telephone']) !== 10)
    {
          $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille ou format téléphone non valide !</div>';
     }
    //------------------------------------------------------
    //contrôler que le mdp soit confirmé        
    /* if($_POST['mdp'] !== $_POST['mdp_confirm'])
     {
         $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Les deux mots passe ne sont pas identiques.</div>';
     } */
    //------------------------------------------------------
	
   if(empty($erreur)) // si la variable $erreur est vide, c'est que nous ne sommes pas entreé dans les condotions IF, l'internaute a bien rempli le formulaire, nous pouvons donc executer l'insertion
    {
		//$hash=$resultat->fetch(PDO::FETCH_ASSOC)['mdp'];
       // $mdp = password_hash($_POST['mdp'], PASSWORD_DEFAULT); // password_hash() permet de créer un clé de hachage, on ne garde jamais en clair les mot de passe dans la BDD
        
        
        $donnees = $pdo->prepare("UPDATE membre SET pseudo = :pseudo, civilite = :civilite, nom = :nom, prenom = :prenom, email = :email, telephone = :telephone WHERE id_membre = :id_membre");
        
        $donnees->bindValue(':pseudo', $_POST['pseudo'], PDO::PARAM_STR);
        $donnees->bindValue(':nom', strtolower($_POST['nom']), PDO::PARAM_STR);
        $donnees->bindValue(':prenom', strtolower($_POST['prenom']), PDO::PARAM_STR);
        $donnees->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $donnees->bindValue(':civilite', $_POST['civilite'], PDO::PARAM_STR);
        $donnees->bindValue(':telephone', $_POST['telephone'], PDO::PARAM_STR);
		$donnees->bindValue(':id_membre',$id_membre_session);
		//$resultat->bindValue(':date_enregistrement', NOW());
        
        $donnees->execute();
        
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">Vos informations ont bien été modifiées <strong class="text-success"> ' . $_POST['pseudo'] . '</strong> ont bien été modifiées!!</div>';
		
		//header("location:profil.php");
    }
    
	
		
		
		
		
        
        
	}
	
require_once("inc/header.inc.php");
//debug($_SESSION);

$resultat = $pdo->prepare("SELECT * FROM membre WHERE id_membre = $id_membre_session ");
$resultat->execute();
$membre = $resultat->fetch(PDO::FETCH_ASSOC);
if ($membre['civilite'] == 'm' ) {$civilite ='Monsieur';}else{ $civilite='Madame';}

?>

<div class="col-md-8 col-md-offset-2">
    <div class="panel-default border">
        <div class="panel-default"><h3 class="text-center">VOS INFORMATIONS</h3></div>
            <div class="panel-body">
                <div class="col-md-12 text-center">
                <ul class="list-unstyled">   
                    <h2>Bonjour <span class="text-danger"><?= $membre['pseudo'] ?></span></h2>
					<li>Civilité : <?= $civilite; ?></li>
                    <li>Nom : <?= ucfirst($membre['nom']); ?></li>
                    <li>Prénom : <?= ucfirst($membre['prenom']);  ?></li>
                    <li>Email : <?= $membre['email']; ?></li>
                    <li>Téléphone : <?= $membre['telephone']; ?></li>
					<li>Vous êtes inscrit depuis le : <?= $membre['date_enregistrement']; ?></li>
					<li>Pour changer votre mot de passe, cliquez <a href="modif_mdp.php">ici</a></li>
                </ul> 
                </div>
            </div>
    </div>
	
	<div class="list-group">
		<h3 class="list-group-item active text-center">Modifier vos informations personnelles</h3> 
		<hr>
	</div>
</div>
<?php
	if($_POST){
	echo $erreur;
    echo $content;
}
	?>
<form method="post" action="" enctype="multipart/form-data" class="col-md-8 col-md-offset-2">
        <input type="hidden" id="id_membre" name="id_membre" value="' . $id_membre_session. '">
      <div class="form-group">
        <label for="pseudo">Pseudo</label>
        <input type="text" class="form-control" id="pseudo" name="pseudo" placeholder="pseudo" value="<?=  $membre['pseudo']?>">
      </div>
	  <div class="form-group">
    <?php echo '<label for="civilite">Civilité</label><br>
	  <select id="civilite" name="civilite" class="form-control">
		<option value="m"'; if ($membre['civilite']=='m'){ echo'selected';} echo'> Homme
		<option value="f"'; if ($membre['civilite']=='f'){ echo'selected';} echo'> Femme
	</select>';?>
	</div>  
      <div class="form-group">
        <label for="nom">Nom</label>
        <input type="text" class="form-control" id="nom" name="nom" placeholder="nom" value="<?= ucfirst($membre['nom']) ?>">
      </div>
      <div class="form-group">
        <label for="prenom">Prénom</label>
        <input type="text" class="form-control" id="prenom" name="prenom" placeholder="prenom" value="<?=  ucfirst($membre['prenom']) ?>">
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="text"class="form-control" id="email" name="email" placeholder="email" value="<?=  $membre['email'] ?>">
      </div>
		<div class="form-group">
        <label for="telephone">Téléphone</label>
        <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Téléphone" value="<?= $membre['telephone'] ?>">
      </div>  
		
	
  
      <button type="submit" class="btn btn-primary col-md-12" name="modifier" id="modifier">Modifier</button>
    </form></div>
<?php


        $resultat = $pdo->prepare("SELECT * FROM membre WHERE id_membre = $id_membre_session");
        $resultat->execute();
        
        $membre = $resultat->fetch(PDO::FETCH_ASSOC);
        //debug($membre);

    

require_once("inc/footer.inc.php");