<?php
require_once("inc/init.inc.php");

/* if(!internauteEstConnecteEtEstAdmin()) // si l'internaute n'est pas ADMIN, il n'a rien faire la, on le redirige vers la page connexion
{
    header("location:" . URL . "connexion.php");
} */
	//$resultat = $pdo->prepare("SELECT * FROM membre");
	
	//$resultat->execute();
   $resultat = executeRequete("SELECT * FROM annonce"); // sélectionne tous les produits
	
	$contenu .= 'Nombre de produits dans la boutique : ' . $resultat->rowCount();
	
	$contenu .= '<table class="table">';
		// Affichage des entêtes du tableau :
		$contenu .= '<tr>';
			$contenu .= '<th>Titre</th>';
			$contenu .= '<th>Description courte</th>';
			$contenu .= '<th>Description longue</th>';
			$contenu .= '<th>Prix</th>';
			$contenu .= '<th>photo</th>';
			$contenu .= '<th>Adresse</th>';
			$contenu .= '<th>Code postal</th>';
			$contenu .= '<th>Ville</th>';
			$contenu .= '<th>Pays</th>';
			$contenu .= '<th>Posté par membre</th>';
			$contenu .= '<th>Catégorie</th>';
		$contenu .= '</tr>';
	
		// Affichage des lignes du tableau :
		while ($annonce = $resultat->fetch(PDO::FETCH_ASSOC)) {
			 debug($annonce);
			$contenu .= '<tr>';
				// on parcourt les informations du tableau associatif $produit :
				foreach($annonce as $indice => $information) {
					if ($indice == 'photo') { // pour la photo on met une balise img :
						$contenu .= '<td><img src="../'. $information .'" width="90" height="90"></td>';
					} else {
						// pour les autres champs :
						$contenu .= '<td>'. $information .'</td>';
					}
				}
			
				$contenu .= '<td>
								<a href="?action=modification&id_produit='. $annonce['id_annonce'] .'">modifier </a>
								-
								<a href="?action=suppression&id_produit='. $annonce['id_annonce'] .'"   onclick="return(confirm(\'Etes-vous sûr de vouloir supprimer cet article ?\'));"  > supprimer</a>
							 </td>';
			$contenu .= '</tr>';
		}
	$contenu .= '</table>';
	
    
    
/*     
} */

require_once("inc/header.inc.php");
echo $content;
echo $contenu;
//debug($_POST);
//debug($_FILES);

/* if(isset($_GET['action']) && ($_GET['action'] == 'ajout' || $_GET['action'] == 'modification'))
{
    if(isset($_GET['id_produit']))
    {
        $resultat = $pdo->prepare("SELECT * FROM produit WHERE id_produit = :id_produit");
        $resultat->bindValue(':id_produit', $_GET['id_produit'], PDO::PARAM_INT);
        $resultat->execute();
        
        $produit_actuel = $resultat->fetch(PDO::FETCH_ASSOC);
        //debug($produit_actuel);
        
        foreach($produit_actuel as $indice => $valeur)
        {
            //debug($indice);
            $$indice = (isset($produit_actuel["$indice"])) ? $produit_actuel["$indice"] : ''; 
        }
    }
    else
    {
            $resultat = $pdo->query("SELECT * FROM produit LIMIT 0,1");
           
            $produit = $resultat->fetch(PDO::FETCH_ASSOC);
            foreach($produit as $indice => $valeur)
            {
                $$indice = '';
            }
    }
    
    echo '<form method="post" action="" enctype="multipart/form-data" class="col-md-8 col-md-offset-2">
        <h2 class="text-center">' . ucfirst($_GET['action']) . ' du produit</h2>
        
        <input type="hidden" id="id_produit" name="id_produit" value="' . $id_produit . '">
      <div class="form-group">
        <label for="reference">Référence</label>
        <input type="pseudo" class="form-control" id="reference" name="reference" placeholder="reference" value="' . $reference . '">
      </div>
      <div class="form-group">
        <label for="categorie">Catégorie</label>
        <input type="text" class="form-control" id="categorie" name="categorie" placeholder="categorie" value="' . $categorie . '">
      </div>
      <div class="form-group">
        <label for="titre">Titre</label>
        <input type="text" class="form-control" id="titre" name="titre" placeholder="titre" value="' . $titre . '">
      </div>
      <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" rows="3" id="description" name="description">' . $description . '</textarea>
      </div>    
      <div class="form-group">
        <label for="couleur">Couleur</label>
        <input type="text" class="form-control" id="couleur" name="couleur" placeholder="couleur" value="' . $couleur . '">
      </div>
      <div class="form-group">
        <label for="taille">Taille</label>
        <select name="taille" class="form-control">
          <option value="s"'; if($taille == 's') echo 'selected'; echo '>S</option>
          <option value="m"'; if($taille == 'm') echo 'selected'; echo '>M</option>
          <option value="l"'; if($taille == 'l') echo 'selected'; echo '>L</option>
          <option value="xl"'; if($taille == 'xl') echo 'selected'; echo '>XL</option>
        </select>
      </div>
      <div class="form-group">
        <label for="public">Public</label>
        <select name="public" class="form-control">
          <option value="m"'; if($public == 'm') echo 'selected'; echo '>Homme</option>
          <option value="f"'; if($public == 'f') echo 'selected'; echo '>Femme</option>
          <option value="mixte"'; if($public == 'mixte') echo 'selected'; echo '>Mixte</option>
        </select>
      </div> 
      <div class="form-group">
        <label for="photo">Photo</label>
        <input type="file" id="photo" name="photo"><br>';
      if(!empty($photo))
      {
          echo '<em>Vous pouvez uploader une nouvelle photo si vous souhaitez la changer</em><br>';
          echo '<img src="' . $photo . '" width="90" height="90">';
      }
      echo '<input type="hidden" id="photo_actuelle" name="photo_actuelle" value="' . $photo . '">';    
      echo '</div>    
      <div class="form-group">
        <label for="prix">Prix</label>
        <input type="text" class="form-control" id="prix" name="prix" placeholder="prix" value="' . $prix . '">
      </div>
      <div class="form-group">
        <label for="stock">Stock</label>
        <input type="text" class="form-control" id="stock" name="stock" placeholder="stock" value="' . $stock . '">
      </div>    
      <button type="submit" class="btn btn-primary col-md-12">' . ucfirst($_GET['action']) . ' du produit</button>
    </form>';
} */


require_once("inc/footer.inc.php");