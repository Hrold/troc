<?php
require_once("inc/init.inc.php");

if(isset($_GET['action']) && $_GET['action'] == 'deconnexion') // si on clique lien 'deconnexion' on détruit la session
{
    session_destroy();
}

if(internauteEstConnecte()) // si l'internaute est connecté, il n'a rien à faire sur la page connexion, on le redirige vers sa page profil
{
    header("location:profil.php");
}

if($_POST)
{
    $resultat = $pdo->prepare("SELECT * FROM membre WHERE email = :email");
    $resultat->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
    $resultat->execute(); // On selectionne en BDD tous les membres potentiel qui possède le même email que l'internaute a saisie dans le formulaire
    
    if($resultat->rowCount() != 0) // si le resultat est est différent de 0, c'est que l'email est connu en BDD 
    {
        $membre = $resultat->fetch(PDO::FETCH_ASSOC); // on associe une méthode pour recolter toute les données de l'internaute qui a saisie le bon email
        //debug($membre);
		
		//debug($_POST);
        // $membre['mdp'] == $_POST['mdp']
		//$mdp = password_verify($_POST['mdp'], $membre['mdp']);
       // vérifie si  une chaine de caractère correspond à une clé de hachage
        if(password_verify($_POST['mdp'], $membre['mdp']) ) // on vérifie que le mdp de la BDD correspond bien au mot de passe que l'internaute a saisie dans le formulaire
        {
            foreach($membre as $indice => $valeur)// on passe en revue les données du membre
            {
                if($indice != 'mdp')// on eclu le mdp qui n'est pas conservé dans le fichier session
                {
                    $_SESSION['membre'][$indice] = $valeur; // on créer un tableau membre dans le fichier session et on enregistre les données  

                }
            }
            //debug($_SESSION);
			 //$_SERVER['HTTP_REFERER'];
            header("location:profil.php"); // ayant les bons identifiants , on le redirige vers sa page profil
        }
        else
        {
            $content .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Mot de passe erroné !</div>';
			//echo $mdp;
        }
    }
    else
    {
        $content .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Email inconnu</div>';
    }
}

require_once("inc/header.inc.php");
echo $content;
?>

<form method="post" action="" class="col-md-8 col-md-offset-2">
    <h2 class="text-center">CONNEXION</h2>
  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="email">
  </div>
  <div class="form-group">
    <label for="mdp">Mot de passe</label>
    <input type="password" class="form-control" id="mdp" name="mdp" placeholder="mdp">
  </div>
  <button type="submit" class="btn btn-primary col-md-12">Connexion</button>    
</form>    

<?php
require_once("inc/footer.inc.php");