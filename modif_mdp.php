<?php
require_once("inc/init.inc.php");
$id_membre_session = $_SESSION['membre']['id_membre'];
$pseudo_membre_session = $_SESSION['membre']['pseudo'];
if(isset($_GET['action']) && $_GET['action'] == 'deconnexion') // si on clique lien 'deconnexion' on détruit la session
{
    session_destroy();
}

if(!internauteEstConnecte()) // si l'internaute est connecté, il n'a rien à faire sur la page connexion, on le redirige vers sa page profil
{
    header("location:connexion.php");
}

if(!empty($_POST))
    {
		
		 $erreur = '';
    
    
    //------------------------------------------------------
     if(strlen($_POST['mdp']) < 4 || strlen($_POST['mdp']) > 20)
     {
          $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille de mot de passe non valide, doit contenir entre 4 et 20 caractères!</div>';
    }
  
    //contrôler que le mdp soit confirmé        
    if($_POST['mdp'] !== $_POST['mdp_confirm'])
     {
         $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Les deux mots passe ne sont pas identiques.</div>';
     }
    //------------------------------------------------------
	
   if(empty($erreur)) // si la variable $erreur est vide, c'est que nous ne sommes pas entreé dans les condotions IF, l'internaute a bien rempli le formulaire, nous pouvons donc executer l'insertion
    {
		//$hash=$resultat->fetch(PDO::FETCH_ASSOC)['mdp'];
       $mdp = password_hash($_POST['mdp'], PASSWORD_DEFAULT); // password_hash() permet de créer un clé de hachage, on ne garde jamais en clair les mot de passe dans la BDD
        
        
        $donnees = $pdo->prepare("UPDATE membre SET mdp = :mdp WHERE id_membre = :id_membre");
        
        $donnees->bindValue(':mdp', $mdp, PDO::PARAM_STR);
		$donnees->bindValue(':id_membre',$id_membre_session);
		//$resultat->bindValue(':date_enregistrement', NOW());
        
        $donnees->execute();
        
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center"> <strong class="text-success"> ' . $pseudo_membre_session . '</strong>, votre mot de passe a bien été modifié!! Vous allez être redirigé sur la page précédente sous 4 secondes.Si tel n\'est pas le cas, cliquez <strong><a href="profil.php">ici</a></strong></div>';
		header('Refresh:4 ; profil.php');
    }
    
	
		
		
		
		
        
        
	}

require_once("inc/header.inc.php");
echo $content;
?>

<form method="post" action="" class="col-md-8 col-md-offset-2">
    <h2 class="text-center">Modification du mot de passe</h2>
  <div class="form-group">
    <label for="mdp">Mot de passe</label>
    <input type="password" class="form-control" id="mdp" name="mdp" placeholder="Insérez votre mot de passe">
  </div>
  <div class="form-group">
    <label for="mdp_confirm">Confirmation du mot de passe</label>
    <input type="password" class="form-control" id="mdp_confirm" name="mdp_confirm" placeholder="Confirmation du mot de passe">
  </div>
  <button type="submit" class="btn btn-primary col-md-12">Modifier</button>    
</form>    

<?php
require_once("inc/footer.inc.php");