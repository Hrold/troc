
-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `id_commentaire` int(3) NOT NULL AUTO_INCREMENT,
  `membre_id` int(3) NOT NULL,
  `annonce_id` int(3) NOT NULL,
  `commentaire` text COLLATE latin1_general_ci NOT NULL,
  `date_enregistrement` datetime NOT NULL,
  PRIMARY KEY (`id_commentaire`),
  KEY `membre_id` (`membre_id`),
  KEY `annonce_id` (`annonce_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id_commentaire`, `membre_id`, `annonce_id`, `commentaire`, `date_enregistrement`) VALUES
(1, 7, 2, 'Vendeur à l\'écoute', '2018-05-06 09:15:32'),
(2, 1, 2, 'tata toto', '2018-05-21 02:14:10'),
(3, 1, 2, 'bo', '2018-05-21 15:32:11'),
(4, 1, 2, 'good boy', '2018-05-21 15:32:46'),
(5, 1, 2, 'good boy', '2018-05-21 15:45:39'),
(6, 1, 2, 'bibi', '2018-05-21 16:42:32'),
(7, 6, 2, 'zozoz', '2018-05-21 17:51:42'),
(8, 6, 2, 'zozoz', '2018-05-21 17:54:51'),
(11, 1, 2, 'zap', '2018-05-21 18:05:08'),
(12, 1, 20, 'plop', '2018-05-21 23:56:40'),
(13, 1, 24, 'pop', '2018-05-22 00:01:38'),
(14, 1, 19, 'beau', '2018-05-22 00:02:43'),
(15, 1, 2, 'qui', '2018-05-22 00:03:56'),
(16, 1, 19, 'bobob', '2018-05-22 00:16:19');
