
-- --------------------------------------------------------

--
-- Structure de la table `membre`
--

DROP TABLE IF EXISTS `membre`;
CREATE TABLE IF NOT EXISTS `membre` (
  `id_membre` int(3) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(20) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `civilite` enum('m','f') NOT NULL,
  `statut` int(1) NOT NULL DEFAULT '0',
  `date_enregistrement` datetime NOT NULL,
  PRIMARY KEY (`id_membre`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `membre`
--

INSERT INTO `membre` (`id_membre`, `pseudo`, `mdp`, `nom`, `prenom`, `telephone`, `email`, `civilite`, `statut`, `date_enregistrement`) VALUES
(1, 'Bobo', '$2y$10$qzRqCvzdeiVeAhI6ZprEmOr9ARhQYa/CIhUsdQmHgitSD7rUuCL3e', 'Bibo', 'Paul', '0101010102', 'bobo@free.fr', 'f', 1, '2018-04-19 13:14:35'),
(6, 'Paul', '$2y$10$4xxjiE4FB5wko3sPqOIcWeL.0pXq.eBo7Bp40xUBIyVi.By6qWSKq', 'Paul', 'Paul', '0101010101', 'paul@paul.fr', 'm', 1, '2018-04-23 10:11:44'),
(7, 'Roro', '$2y$10$A8oU4VxYlGxGvP9mTHTh/u/KrzUtqqUyvOVtgoy4iw9LrctW5kQVO', 'Roro', 'Ror', '0202020203', 'roro@roro.be', 'f', 1, '2018-05-06 00:00:00'),
(8, 'zozo', '$2y$10$A8oU4VxYlGxGvP9mTHTh/u/KrzUtqqUyvOVtgoy4iw9LrctW5kQVO', 'zozo', 'zozo', '0102030405', 'zozo@zozo.fr', 'm', 0, '2018-05-19 14:54:05');
