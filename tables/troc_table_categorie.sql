
-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(3) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `motscles` text NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `titre`, `motscles`) VALUES
(1, 'emploi', 'Offres d\'emploi'),
(2, 'vehicule', 'Voiture, Motos, Bateaux, Vélos, Equipement'),
(3, 'immobilier', 'Ventes, Locations, Colocations, Bureaux, Logement'),
(4, 'vacances', 'Camping, Hôtels, Hôte'),
(5, 'multimédia', 'Jeux vidéos, Informatique, Image, Son, Téléphone'),
(6, 'loisirs', 'Films, Musique, Livres'),
(7, 'matériel', 'Outillage, Fourniture de bureau, Matériel agricole'),
(8, 'services', 'Prestations de services, Evénements'),
(9, 'maison', 'Ameublement, Électroménager, Bricolage, Jardinage'),
(10, 'vetements', 'Jean, Chemise, Robe, Chaussure'),
(11, 'autres', '');
