
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD CONSTRAINT `annonce_categorie_fk` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id_categorie`) ON UPDATE CASCADE,
  ADD CONSTRAINT `annonce_membre_fk` FOREIGN KEY (`membre_id`) REFERENCES `membre` (`id_membre`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `commentaire_annonce_fk` FOREIGN KEY (`annonce_id`) REFERENCES `annonce` (`id_annonce`) ON UPDATE CASCADE,
  ADD CONSTRAINT `commentaire_membre_fk` FOREIGN KEY (`membre_id`) REFERENCES `membre` (`id_membre`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `note_membre1_fk` FOREIGN KEY (`membre_id1`) REFERENCES `membre` (`id_membre`) ON UPDATE CASCADE,
  ADD CONSTRAINT `note_membre2_fk` FOREIGN KEY (`membre_id2`) REFERENCES `membre` (`id_membre`) ON UPDATE CASCADE;
