
-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

DROP TABLE IF EXISTS `annonce`;
CREATE TABLE IF NOT EXISTS `annonce` (
  `id_annonce` int(3) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `description_courte` varchar(255) NOT NULL,
  `description_longue` text NOT NULL,
  `prix` int(8) NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `pays` varchar(20) NOT NULL,
  `ville` varchar(20) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `cp` int(5) NOT NULL,
  `membre_id` int(3) NOT NULL,
  `photo_id` int(3) DEFAULT NULL,
  `categorie_id` int(3) NOT NULL,
  `date_enregistrement` datetime NOT NULL,
  PRIMARY KEY (`id_annonce`),
  KEY `membre_id` (`membre_id`),
  KEY `categorie_id` (`categorie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `annonce`
--

INSERT INTO `annonce` (`id_annonce`, `titre`, `description_courte`, `description_longue`, `prix`, `photo`, `pays`, `ville`, `adresse`, `cp`, `membre_id`, `photo_id`, `categorie_id`, `date_enregistrement`) VALUES
(2, 'iphone 5s', 'iphone 5 s noir', 'iphone 5 s noir, jamais servi dans son emballage d\'origine, occasion a saisir de suite', 500, 'images/product_1.png', 'france', 'sevry', 'rue des bouletes', 86505, 1, NULL, 5, '2018-04-25 07:06:30'),
(19, 'test', 'test', 'mkdlsjgfdsgklfghjdnfklg', 50, '', 'France', 'versailles', 'rue', 78000, 1, NULL, 5, '2018-04-26 09:08:56'),
(20, 'test', 'kejfsekfmsdkfkf', 'cvdfùl;mdflg', 50, 'http://localhost/Troc2/images/1-1525196520-Screenshot_20180302-114415.png', 'france', 'Suresnes', 'rue', 88000, 1, NULL, 1, '2018-05-01 19:42:00'),
(22, 'test', 'xdffdfdd', 'dffddghhhd', 540, 'http://localhost/Troc2/images/1-1525196885-Screenshot_20180302-114415.png', 'france', 'epinal', 'rue des trous', 79000, 1, NULL, 6, '2018-05-01 19:48:05'),
(23, 'moi', 'roi', 'fnekzlf,ezmkl', 58, 'http://localhost/Troc2/images/6-1525611531-Screenshot_20180302-114415.png', 'France', 'Suresnes', '62 bd du mal de lattre de tassigny', 92150, 6, NULL, 1, '2018-05-06 14:58:51'),
(24, 'Belle voiture', 'Voiture pô cher', 'Voiture servant pas souvent, alors je la vends, si tu laveux, elle est tout à toi', 15000, 'http://localhost/Troc2/images/6-1526915539-car-3415288__480.jpg', 'France', 'Paris', 'rue des anglais', 75001, 6, NULL, 2, '2018-05-21 17:12:19');
