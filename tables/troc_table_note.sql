
-- --------------------------------------------------------

--
-- Structure de la table `note`
--

DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
  `id_note` int(3) NOT NULL AUTO_INCREMENT,
  `membre_id1` int(3) NOT NULL,
  `membre_id2` int(3) NOT NULL,
  `note` int(3) NOT NULL,
  `avis` text,
  `date_enregistrement` datetime NOT NULL,
  PRIMARY KEY (`id_note`),
  KEY `membre_id2` (`membre_id2`),
  KEY `membre_id1` (`membre_id1`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `note`
--

INSERT INTO `note` (`id_note`, `membre_id1`, `membre_id2`, `note`, `avis`, `date_enregistrement`) VALUES
(1, 7, 1, 5, 'Reponds aux questions en temps et en heure', '2018-05-06 10:29:23'),
(4, 6, 1, 2, 'pipo', '2018-05-21 18:09:34'),
(16, 1, 6, 5, '', '2018-05-22 00:17:36'),
(19, 1, 6, 4, '', '2018-05-22 00:48:03');
