<?php
require_once("../inc/init.inc.php");

if(!internauteEstConnecteEtEstAdmin()) // si l'internaute n'est pas ADMIN, il n'a rien faire la, on le redirige vers la page connexion
{
    header("location:" . URL . "connexion.php");
}

//-------- SUPPRESSION annonce --------------//
if(isset($_GET['action']) && $_GET['action'] == 'suppression')
{
    // Exercice : requete de suppression annonce
    $resultat = $pdo->prepare("DELETE FROM annonce WHERE id_annonce = :id_annonce");
    $resultat->bindValue(':id_annonce', $_GET['id_annonce'], PDO::PARAM_INT);
    $resultat->execute();
    
    $_GET['action'] = 'affichage';
    
    $content .= '<div class="alert alert-success col-md-8 col-md-offset-2 text-center">Le annonce n° ' . $_GET['id_annonce'] . ' a bien été supprimé! </div>';
    
}


if(!empty($_POST))
{
    $photo_bdd = '';
    if(isset($_GET['action']) && $_GET['action'] == 'modification')
    {
        $photo_bdd = $_POST['photo_actuelle'];
    }
    if(!empty($_FILES['photo']['name']))
    {
        $nom_photo = $_POST['reference'] . '-' . $_FILES['photo']['name']; 
        
        $photo_bdd = URL . "photo/$nom_photo";
        //echo $photo_bdd;
        
        $photo_dossier = RACINE_SITE . "photo/$nom_photo";
        //echo $photo_dossier;
        
        copy($_FILES['photo']['tmp_name'], $photo_dossier);
    }
    
    if(isset($_GET['action']) && $_GET['action'] == 'ajout')
    {
        // Réaliser le traitement de l'insertion de la photo
        $donnees = $pdo->prepare("INSERT INTO annonce (id_annonce,titre, description_courte, description_longue, prix, photo, pays, ville, adresse, cp, membre_id, categorie_id) VALUES(:id_annonce,:titre, :description_courte, :description_longue, :prix, :photo, :pays, :ville, :adresse, :cp, :membre_id, :categorie_id)");
        
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">Le annonce reference <strong class="text-success"> ' . $_POST['reference'] . '</strong> a bien été ajouté à la boutique</div>';
    }
    else
    {
        // Requete de modification
        $donnees = $pdo->prepare("UPDATE annonce SET titre = :titre, description_courte = :description_courte, description_longue = :description_longue, prix = :prix, photo = :photo, pays = :pays, ville = :ville, adresse = :adresse, cp = :cp, membre_id = :membre_id, categorie_id = :categorie_id  WHERE id_annonce = $_POST[id_annonce]");
        
        $_GET['action'] = 'affichage';
        
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">Le annonce reference <strong class="text-success"> ' . $_POST['reference'] . '</strong> a bien modifié!!</div>';
    }
    
    
    $resultat =  $pdo->query("SELECT * FROM annonce");
    
    for($i = 0; $i < $resultat->columnCount(); $i++)
    {
        $colonne = $resultat->getColumnMeta($i);
        //debug($colonne);
        if($colonne['name'] != 'id_annonce')
        {
            if($colonne['name'] == 'photo')
            {
                $donnees->bindValue(":$colonne[name]", $photo_bdd, PDO::PARAM_STR);
            }
            else
            {
                $donnees->bindValue(":$colonne[name]", $_POST["$colonne[name]"], PDO::PARAM_STR);
            }
        }    
    }
    
    $donnees->execute(); 
    
}

//---- LIENS annonceS
$content .= '<div class="list-group col-md-6 col-md-offset-3">';
$content .= '<h3 class="list-group-item active text-center">BACK OFFICE</h3>';
$content .= '<a href="?action=affichage" class="list-group-item text-center">Affichage annonces</a>';
$content .= '<a href="?action=ajout" class="list-group-item text-center">Ajout annonce</a>';
$content .= '<hr></div>';

//---- AFFICHAGE annonceS
if(isset($_GET['action']) && $_GET['action'] == 'affichage')
{
    $resultat = $pdo->query("SELECT * FROM annonce");
    $content .= '<div class="col-md-10 col-md-offset-1 text-center"><h3 class="text-center">Affichage annonces</h3>';
    
    $content .= 'Nombre de annonce(s) dans la boutique <span class="badge">' . $resultat->rowCount() . '</span></div>';
    
    $content .= '<table class="col-md-10 table" style="margin-top: 10px;"><tr class="active">';
    for($i = 0; $i < $resultat->columnCount(); $i++)
    {
        $colonne = $resultat->getColumnMeta($i);
        $content .= '<th>' . $colonne['name'] . '</th>';
    }
    $content .= '<th>Modification</th>';
    $content .= '<th>Suppression</th>';
    $content .= '</tr>';
    while($annonce =  $resultat->fetch(PDO::FETCH_ASSOC))
    {
        $content .= '<tr>';
        //debug($annonce);
        foreach($annonce as $indice => $infos)
        {
            
            if($indice == 'photo')
            {
                $content .= '<td><img src="' . $infos . '" width="70" height="70"></td>'; 
            }
            else
            {
                $content .= '<td>' . $infos . '</td>'; 
            }
        }
        $content .= '<td class="text-center"><a href="?action=modification&id_annonce=' . $annonce['id_annonce'] . '"><span class="glyphicon glyphicon-pencil"></span></a></td>';
        
        $content .= '<td class="text-center"><a href="?action=suppression&id_annonce=' . $annonce['id_annonce'] . '" onClick="return(confirm(\'En êtes certain ?\'));"><span class="glyphicon glyphicon-trash"></span></a></td>';
        $content .= '</tr>';
    }
    $content .= '</table>';
    
    
    
}

require_once("../inc/header.inc.php");
echo $content;
//debug($_POST);
//debug($_FILES);

if(isset($_GET['action']) && ($_GET['action'] == 'ajout' || $_GET['action'] == 'modification'))
{
    if(isset($_GET['id_annonce']))
    {
        $resultat = $pdo->prepare("SELECT * FROM annonce WHERE id_annonce = :id_annonce");
        $resultat->bindValue(':id_annonce', $_GET['id_annonce'], PDO::PARAM_INT);
        $resultat->execute();
        
        $annonce_actuel = $resultat->fetch(PDO::FETCH_ASSOC);
        //debug($annonce_actuel);
        
        foreach($annonce_actuel as $indice => $valeur)
        {
            //debug($indice);
            $$indice = (isset($annonce_actuel["$indice"])) ? $annonce_actuel["$indice"] : ''; 
        }
    }
    else
    {
            $resultat = $pdo->query("SELECT * FROM annonce LIMIT 0,1");
           
            $annonce = $resultat->fetch(PDO::FETCH_ASSOC);
            foreach($annonce as $indice => $valeur)
            {
                $$indice = '';
            }
    }
    
    echo '<form method="post" action="" enctype="multipart/form-data" class="col-md-8 col-md-offset-2">
        <h2 class="text-center">' . ucfirst($_GET['action']) . ' du annonce</h2>
        
        <input type="hidden" id="id_annonce" name="id_annonce" value="' . $id_annonce . '">
      <div class="form-group">
        <label for="titre">Titre</label>
        <input type="titre" class="form-control" id="titre" name="titre" placeholder="titre" value="' . $titre . '">
      </div>
      <div class="form-group">
        <label for="categorie">Catégorie</label>
        <input type="text" class="form-control" id="categorie" name="categorie" placeholder="categorie" value="' . $categorie . '">
      </div>
      <div class="form-group">
        <label for="titre">Titre</label>
        <input type="text" class="form-control" id="titre" name="titre" placeholder="titre" value="' . $titre . '">
      </div>
      <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" rows="3" id="description" name="description">' . $description . '</textarea>
      </div>    
      <div class="form-group">
        <label for="couleur">Couleur</label>
        <input type="text" class="form-control" id="couleur" name="couleur" placeholder="couleur" value="' . $couleur . '">
      </div>
      <div class="form-group">
        <label for="taille">Taille</label>
        <select name="taille" class="form-control">
          <option value="s"'; if($taille == 's') echo 'selected'; echo '>S</option>
          <option value="m"'; if($taille == 'm') echo 'selected'; echo '>M</option>
          <option value="l"'; if($taille == 'l') echo 'selected'; echo '>L</option>
          <option value="xl"'; if($taille == 'xl') echo 'selected'; echo '>XL</option>
        </select>
      </div>
      <div class="form-group">
        <label for="public">Public</label>
        <select name="public" class="form-control">
          <option value="m"'; if($public == 'm') echo 'selected'; echo '>Homme</option>
          <option value="f"'; if($public == 'f') echo 'selected'; echo '>Femme</option>
          <option value="mixte"'; if($public == 'mixte') echo 'selected'; echo '>Mixte</option>
        </select>
      </div> 
      <div class="form-group">
        <label for="photo">Photo</label>
        <input type="file" id="photo" name="photo"><br>';
      if(!empty($photo))
      {
          echo '<em>Vous pouvez uploader une nouvelle photo si vous souhaitez la changer</em><br>';
          echo '<img src="' . $photo . '" width="90" height="90">';
      }
      echo '<input type="hidden" id="photo_actuelle" name="photo_actuelle" value="' . $photo . '">';    
      echo '</div>    
      <div class="form-group">
        <label for="prix">Prix</label>
        <input type="text" class="form-control" id="prix" name="prix" placeholder="prix" value="' . $prix . '">
      </div>
      <div class="form-group">
        <label for="stock">Stock</label>
        <input type="text" class="form-control" id="stock" name="stock" placeholder="stock" value="' . $stock . '">
      </div>    
      <button type="submit" class="btn btn-primary col-md-12">' . ucfirst($_GET['action']) . ' du annonce</button>
    </form>';
}


require_once("../inc/footer.inc.php");