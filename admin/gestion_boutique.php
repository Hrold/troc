<?php
require_once('../inc/init.inc.php');

//----------------- TRAITEMENT ------------------
// 1- vérification que le membre est admin :
if (!internauteEstConnecteEtEstAdmin()) {
	// si membre non connecté ou non admin on le redirige vers la page de connexion :	
	header('location:../connexion.php');  // on demande la page de connexion
	exit();  // on quitte le script
}

$valSession = $_SESSION['membre']['id_membre'];

// 7- suppression du produit :
if (isset($_GET['action']) && $_GET['action'] == 'suppression' && isset($_GET['id_annonce'])) {
	// si l'indice action existe et qu'il vaut "suppression", et qu'existe l'indice id_produit :
	$resultat = executeRequete("DELETE FROM annonce WHERE id_annonce = :id_annonce", array(':id_annonce' => $_GET['id_annonce']));
	
	if ($resultat->rowCount() > 0) {
		$contenu .= '<div class="bg-success">Le produit a bien été supprimé !</div>';
	}
	
	$_GET['action'] = 'affichage'; // permet de lancer l'affichage des produits (cf chapitre 6 ci-dessous)
		
}




// 4- Enregistrement du produit en BDD :
if (!empty($_POST)) {  // si le formulaire est soumis
	// debug($_POST);
	
	// ici il faudrait mettre tous les contrôles sur le formulaire...
	
	$photo_bdd = '';  // contiendra le chemin de la photo à insérer en BDD
	
	// 9- fin du traitement de la photo :
	if (isset($_POST['photo_actuelle'])) {
		// si cela existe, c'est que nous sommes en train de modifier la photo : on la remet donc en BDD pour ne pas l'effacer :
		$photo_bdd = $_POST['photo_actuelle'];
		
	}
	
	// 5- traitement de la photo :
	// debug($_FILES);
	if (!empty($_FILES['photo']['name'])) { // si une photo est uploadée, la valeur "name" n'est pas vide
	
		// on crée une variable pour le nom du fichier photo :
		$nom_photo = $_POST['reference'] . '_' . $_FILES['photo']['name']; // on crée un nom de fichier unique par référence de produit
		
		// on crée une variable pour le chemin de la photo à mettre en BDD :
		$photo_bdd = '../' . $nom_photo;  // chemin relatif de la photo qui est enregistré en BDD nécessaire aux balises <img> : photo/nomfichier.jpg
		
		// on crée une variable pour le chemin absolu de la photo physique enregistrée sur notre serveur :
		$photo_physique = $_SERVER['DOCUMENT_ROOT'] . RACINE_SITE . $photo_bdd;  // chemin absolu complet depuis la racine du serveur pour enregistrer le fichier physique : C:/wamp64/www/PHP/08-site/photo/nomfichier.jpg
		
		// $_SERVER['DOCUMENT_ROOT'] est une superglobale qui fournit ici la racine du serveur sur lequel se trouve le site
		
		copy($_FILES['photo']['tmp_name'], $photo_physique);  // copie le fichier qui est temporairement dans $_FILES['photo']['tmp_name'] vers l'emplacement $photo_physique
		
	}
	
	// Enregistrement du produit :
	executeRequete("REPLACE INTO annonce (id_annonce,titre, description_courte, description_longue, prix, photo, pays, ville, adresse, cp, membre_id, categorie_id,date_enregistrement) VALUES(:id_annonce,:titre, :description_courte, :description_longue, :prix, :photo, :pays, :ville, :adresse, :cp, :membre_id, :categorie_id, now())", 
					array( ':id_annonce'  => $_POST['id_annonce'],
							':titre'  => $_POST['titre'],
					       ':description_courte'  => $_POST['description_courte'],
					       ':description_longue'      => $_POST['description_longue'],
					       ':prix'=> $_POST['prix'],
					       ':photo'    => $photo_bdd,
					       ':pays'     => $_POST['pays'],
					       ':ville'     => $_POST['ville'],
					       ':adresse'  => $_POST['adresse'],
						   ':cp'       => $_POST['cp'],
						   ':membre_id'=> $valSession,
						   'categorie_id'	=>	$_POST['categorie_id']
					));
					
		
					
					//debug(executeRequete());
	// Note : bien mettre les champs dans le même ordre que la table "produit" de la BDD car on n'a pas spécifié les champs concernés dans une première paire de ().
	
	$contenu .= '<div class="bg-success">L\'annonce a bien été enregistré.</div>';
	
	$_GET['action'] = 'affichage';  // pour déclencher l'affichage de la table HTML avec tous les produits (cf chapitre 6 ci-dessous)
	
}  // fin du if (!empty($_POST))


// 6- Affichage des produits dans une table HTML :
if (isset($_GET['action']) && $_GET['action'] == 'affichage') {
	// si on a demandé l'affichage de la table HTML :
	
	$resultat = executeRequete("SELECT * FROM annonce"); // sélectionne tous les produits
	
	$contenu .= 'Nombre d\'annonces dans la boutique : ' . $resultat->rowCount();
	
	$contenu .= '<table class="table">';
		// Affichage des entêtes du tableau :
		$contenu .= '<tr>';
			$contenu .= '<th>Titre</th>';
			$contenu .= '<th>Description courte</th>';
			$contenu .= '<th>Description longue</th>';
			$contenu .= '<th>Prix</th>';
			$contenu .= '<th>Photo</th>';
			$contenu .= '<th>Pays</th>';
			$contenu .= '<th>Ville</th>';
			$contenu .= '<th>Adresse</th>';
			$contenu .= '<th>code postal</th>';
			$contenu .= '<th>Catégorie</th>';

		$contenu .= '</tr>';
	
		// Affichage des lignes du tableau :
		while ($produit = $resultat->fetch(PDO::FETCH_ASSOC)) {
			// debug($produit);
			$contenu .= '<tr>';
				// on parcourt les informations du tableau associatif $produit :
				foreach($produit as $indice => $information) {
					if ($indice == 'photo') { // pour la photo on met une balise img :
						$contenu .= '<td><img src="../'. $information .'" width="90" height="90"></td>';
					} elseif ($indice == 'id_annonce'){
						$contenu .='';
					}else {
						// pour les autres champs :
						$contenu .= '<td>'. $information .'</td>';
					}
				}
			
				$contenu .= '<td>
								<a href="?action=modification&id_annonce='. $produit['id_annonce'] .'">modifier </a>
								-
								<a href="?action=suppression&id_annonce='. $produit['id_annonce'] .'"   onclick="return(confirm(\'Etes-vous sûr de vouloir supprimer cet article ?\'));"  > supprimer</a>
							 </td>';
			$contenu .= '</tr>';
		}
	$contenu .= '</table>';
	
} // fin du if (isset($_GET['action']) && $_GET['action'] == 'affichage')
	
	
	


//----------------- AFFICHAGE -------------------
require_once('../inc/header.inc.php');
// 2- Onglets 'ajout' et 'affichage' des produits :
echo '<ul class="nav nav-tabs">
		<li><a href="?action=affichage">Affichage des annonces</a></li>	
		<li><a href="?action=ajout">Ajout produit</a></li>	
      </ul>';

echo $contenu;  // pour afficher les messages

// 3- Formulaire HTML de produit :
if (isset($_GET['action']) && ($_GET['action'] == 'ajout' || $_GET['action'] == 'modification')) :  // syntaxe du if avec ":" et endif à la fin qui remplacent les accolades
// Si je suis en ajout ou modification de produit, j'affiche le formulaire :
	
	// 8- Formulaire de modification avec présaisie des valeurs :
	if (isset($_GET['id_annonce'])) {
		// si existe l'indice id_produit, je peux sélectionner le produit en BDD :
		$resultat = executeRequete("SELECT * FROM annonce WHERE id_annonce = :id_annonce", array(':id_annonce' => $_GET['id_annonce']));	
		
		$produit_actuel = $resultat->fetch(PDO::FETCH_ASSOC);  // pas de boucle sur ce fetch car il n'y a qu'un seul produit par id. $produit_actuel est un array qui contient toutes les infos du produit à mettre dans le formulaire 
		
	}
		
?>	
	<h3>Formulaire de modification d'annonce </h3>
	<form method="post" action="" enctype="multipart/form-data"><!-- multipart/form-data spécifie que ce formulaire envoie des données binaires (= photo) et du texte (= champs du form) : permet d'uploader une photo -->
	
	<input type="hidden" id="id_annonce" name="id_annonce" value="<?php echo $produit_actuel['id_annonce'] ?? ''; ?>"><!-- nécessaire pour la phase de modification d'un produit. Champ caché pour ne pas pouvoir être modifiable -->

	<label for="titre">Titre</label><br>
	<input type="text" id="titre" name="titre" value="<?php echo $produit_actuel['titre'] ?? ''; ?>"><br><br>
	
	<label for="description_courte">Description courte</label><br>
	<input type="text" id="description_courte" name="description_courte" value="<?php echo $produit_actuel['description_courte'] ?? ''; ?>"><br><br>
	
	<label for="description_longue">Description longue</label><br>
	<textarea  id="description_longue" name="description_longue" value="<?php echo $produit_actuel['description_longue'] ?? ''; ?>"></textarea><br><br>

	<label for="prix">Prix</label><br>
	<input type="text" id="prix" name="prix" value="<?php echo $produit_actuel['prix'] ?? ''; ?>"><br><br>
	
	<label for="photo">Photo</label><br>
	<!-- 5 upload de la photo -->
	<input type="file" id="photo" name="photo"><!--  ne pas oublier l'attribut enctype="multipart/form-data" dans la balise form-->
	<!-- 9 modification de la photo -->
	<?php
	if (isset($produit_actuel['photo'])) { // si existe alors on est en train de modifier un produit : on affiche alors la vignette de la photo actuelle :
		echo '<i>Vous pouvez uploader une nouvelle photo</i>';
		echo '<p>Photo actuelle : </p>';
		echo '<img src="../'. $produit_actuel['photo'] .'" name="photo_actuelle" width="90" height="90"><br>';
		echo '<input type="hidden" name="photo_actuelle" value="'. $produit_actuel['photo'] .'"><br>';  // on a besoin de mettre dans le formulaire le chemin de la photo qui vient de la BDD. Cet input complète le $_POST['photo_actuelle'] qui va en base : on prend la photo de la BDD, puis on la met dans le <form>, puis elle va dans le $_POST puis on la remet dans la BDD 
	}
	?>
	
	
		
	<label for="pays">Pays</label><br>
	<input type="text" id="pays" name="pays" value="<?php echo $produit_actuel['pays'] ?? 0; ?>"><br><br>
	
	<label for="ville">Ville</label><br>
	<input type="text" id="ville" name="ville" value="<?php echo $produit_actuel['ville'] ?? 0; ?>"><br><br>
	
	<label for="adresse">Adresse</label><br>
	<input type="text" id="adresse" name="adresse" value="<?php echo $produit_actuel['adresse'] ?? 0; ?>"><br><br>
	
	<label for="cp">Code postal</label><br>
	<input type="text" id="cp" name="cp" value="<?php echo $produit_actuel['cp'] ?? 0; ?>"><br><br>
	
	<label for="categorie_id_categorie">Categorie</label><br>
		
		<?php 
		$tab_multi = executeRequete("SELECT * FROM categorie");
		echo '<select id="id_categorie" name="id_categorie">';
		while ($ensembleC = $tab_multi->fetch(PDO::FETCH_ASSOC)){
	
		echo '<option>'.$ensembleC['id_categorie'].'</option>';
		}
		echo '</select>';
		?>
		<input type="hidden" id="date_enregistrement" name="date_enregistrement" value="<?php echo $annonce_actuel['date_enregistrement'] ?? 0; ?>">
	
	<input type="submit" value="valider" class="btn">
</form>

<?php
endif;
require_once('../inc/footer.inc.php');






