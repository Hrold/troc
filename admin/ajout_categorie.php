<?php
require_once("../inc/init.inc.php");
require_once("../inc/fonctions.inc.php");
if(!internauteEstConnecteEtEstAdmin()) // si l'internaute n'est pas connecté, il n'a rien faire la, on le redirige vers la page connexion
{
    header("location:" . URL . "connexion.php");
}

 $donnees = $pdo->prepare("INSERT INTO categorie (titre, motscles) VALUES(:titre, :motscles");
		$donnees->bindValue(':titre', $_POST['titre']);
		$donnees->bindValue(':motscles', $_POST['motscles']);
        $donnees->execute();
//header("location:" . URL . "admin/gestion_categorie.php");
?>