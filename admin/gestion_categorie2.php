<?php
require_once("../inc/init.inc.php");

if(!internauteEstConnecteEtEstAdmin()) // si l'internaute n'est pas ADMIN, il n'a rien faire la, on le redirige vers la page connexion
{
    header("location:" . URL . "connexion.php");
}

//-------- SUPPRESSION PRODUIT --------------//
if(isset($_GET['action']) && $_GET['action'] == 'suppression')
{
    // Exercice : requete de suppression produit
    $resultat = $pdo->prepare("DELETE FROM categorie WHERE id_categorie = :id_categorie");
    $resultat->bindValue(':id_categorie', $_GET['id_categorie'], PDO::PARAM_INT);
    $resultat->execute();
    
    $_GET['action'] = 'affichage';
    
    $content .= '<div class="alert alert-success col-md-8 col-md-offset-2 text-center">La categorie n° ' . $_GET['id_categorie'] . ' a bien été supprimé! </div>';
    
}


/* if(!empty($_POST))
{
    $photo_bdd = '';
    if(isset($_GET['action']) && $_GET['action'] == 'modification')
    {
        $photo_bdd = $_POST['photo_actuelle'];
    }
    if(!empty($_FILES['photo']['name']))
    {
        $nom_photo = $_POST['reference'] . '-' . $_FILES['photo']['name']; 
        
        $photo_bdd = URL . "photo/$nom_photo";
        //echo $photo_bdd;
        
        $photo_dossier = RACINE_SITE . "photo/$nom_photo";
        //echo $photo_dossier;
        
        copy($_FILES['photo']['tmp_name'], $photo_dossier);
    } */
    
    if(isset($_GET['action']) && $_GET['action'] == 'ajout')
    {
        // Réaliser le traitement de l'insertion
        $donnees = $pdo->prepare("INSERT INTO categorie (titre, motscles)VALUES(:titre, :motscles)");
		//$donnees->bindValue(':titre', $_POST['titre'], PDO::PARAM_STR);
        //$donnees->bindValue(':motscles', $_POST['motscles'], PDO::PARAM_STR);
        $donnees->execute();
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">La catégorie ayant le titre <strong class="text-success"> ' . $_POST['titre'] . '</strong> a bien été ajouté à la boutique</div>';
    }
    else
    {
        // Requete de modification
        $donnees = $pdo->prepare("UPDATE produit SET titre = :titre, motscles = :motscles WHERE id_categorie = $_POST[id_categorie]");
        
        $_GET['action'] = 'affichage';
        
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">La cotégorie ayant le titre <strong class="text-success"> ' . $_POST['titre'] . '</strong> a bien modifié!!</div>';
    }
    
    
    $resultat =  $pdo->query("SELECT * FROM categorie");
    
    for($i = 0; $i < $resultat->columnCount(); $i++)
    {
        $colonne = $resultat->getColumnMeta($i);
        //debug($colonne);
        if($colonne['name'] != 'id_categorie')
        {
                $donnees->bindValue(":$colonne[name]", $_POST["$colonne[name]"], PDO::PARAM_STR);
            
        }    
    }
    
    $donnees->execute(); 
    


//---- LIENS PRODUITS
$content .= '<div class="list-group col-md-6 col-md-offset-3">';
$content .= '<h3 class="list-group-item active text-center">BACK OFFICE</h3>';
$content .= '<a href="?action=affichage" class="list-group-item text-center">Affichage produits</a>';
$content .= '<a href="?action=ajout" class="list-group-item text-center">Ajout produit</a>';
$content .= '<hr></div>';

//---- AFFICHAGE PRODUITS
if(isset($_GET['action']) && $_GET['action'] == 'affichage')
{
    $resultat = $pdo->query("SELECT * FROM categorie");
    $content .= '<div class="col-md-10 col-md-offset-1 text-center"><h3 class="text-center">Affichage Produits</h3>';
    
    $content .= 'Nombre de produit(s) dans la boutique <span class="badge">' . $resultat->rowCount() . '</span></div>';
    
    $content .= '<table class="col-md-10 table" style="margin-top: 10px;"><tr class="active">';
    for($i = 0; $i < $resultat->columnCount(); $i++)
    {
        $colonne = $resultat->getColumnMeta($i);
        $content .= '<th>' . $colonne['name'] . '</th>';
    }
    $content .= '<th>Modification</th>';
    $content .= '<th>Suppression</th>';
    $content .= '</tr>';
    while($produit =  $resultat->fetch(PDO::FETCH_ASSOC))
    {
        $content .= '<tr>';
        //debug($produit);
        foreach($produit as $indice => $infos)
        {
            
            if($indice == 'photo')
            {
                $content .= '<td><img src="' . $infos . '" width="70" height="70"></td>'; 
            }
            else
            {
                $content .= '<td>' . $infos . '</td>'; 
            }
        }
        $content .= '<td class="text-center"><a href="?action=modification&id_categorie=' . $produit['id_categorie'] . '"><span class="glyphicon glyphicon-pencil"></span></a></td>';
        
        $content .= '<td class="text-center"><a href="?action=suppression&id_categorie=' . $produit['id_categorie'] . '" onClick="return(confirm(\'En êtes certain ?\'));"><span class="glyphicon glyphicon-trash"></span></a></td>';
        $content .= '</tr>';
    }
    $content .= '</table>';
    
    
    
}

require_once("../inc/header.inc.php");
echo $content;
//debug($_POST);
//debug($_FILES);

if(isset($_GET['action']) && ($_GET['action'] == 'ajout' || $_GET['action'] == 'modification'))
{
    if(isset($_GET['id_categorie']))
    {
        $resultat = $pdo->prepare("SELECT * FROM categorie WHERE id_categorie = :id_categorie");
        $resultat->bindValue(':id_categorie', $_GET['id_categorie'], PDO::PARAM_INT);
        $resultat->execute();
        
        $produit_actuel = $resultat->fetch(PDO::FETCH_ASSOC);
        //debug($produit_actuel);
        
        foreach($produit_actuel as $indice => $valeur)
        {
            //debug($indice);
            $$indice = (isset($produit_actuel["$indice"])) ? $produit_actuel["$indice"] : ''; 
        }
    }
    else
    {
            $resultat = $pdo->query("SELECT * FROM categorie LIMIT 0,1");
           
            $produit = $resultat->fetch(PDO::FETCH_ASSOC);
            foreach($produit as $indice => $valeur)
            {
                $$indice = '';
            }
    }
    
    echo '<form method="post" action="" enctype="multipart/form-data" class="col-md-8 col-md-offset-2">
        <h2 class="text-center">' . ucfirst($_GET['action']) . ' du produit</h2>
        
        <input type="hidden" id="id_categorie" name="id_categorie" value="' . $id_categorie . '">
      <div class="form-group">
        <label for="titre">Titre</label>
        <input type="titre" class="form-control" id="titre" name="titre" placeholder="titre" value="' . $titre. '">
      </div>
      <div class="form-group">
        <label for="categorie">Mots clés</label>
        <input type="text" class="form-control" id="motscles" name="motscles" placeholder="motscles" value="' . $motscles . '">
      </div>
    
      <button type="submit" class="btn btn-primary col-md-12">' . ucfirst($_GET['action']) . ' de la catégorie</button>
    </form>';
}


require_once("../inc/footer.inc.php");