<?php
require_once("../inc/init.inc.php");

if(!internauteEstConnecteEtEstAdmin()) // si l'internaute n'est pas connecté, il n'a rien faire la, on le redirige vers la page connexion
{
    header("location:" . URL . "connexion.php");
}
$id_membre_session = $_SESSION['membre']['id_membre'];
//-------- SUPPRESSION catégorie --------------//
if(isset($_GET['action']) && $_GET['action'] == 'suppression')
{
    // Exercice : requete de suppression catégorie
    $resultat = $pdo->prepare("DELETE FROM categorie WHERE id_categorie = :id_categorie");
    $resultat->bindValue(':id_categorie', $_GET['id_categorie'], PDO::PARAM_INT);
    $resultat->execute();
    
    $_GET['action'] = 'affichage';
    
    $content .= '<div class="alert alert-success col-md-8 col-md-offset-2 text-center">La categorie n° ' . $_GET['id_categorie'] . ' a bien été supprimé! </div>';
    
}


if(!empty($_POST))
{
    //$erreur='';
    if(isset($_GET['action']) && ($_GET['action'] == 'ajout'))
    {	
		$verif_categorie = $pdo->prepare("SELECT * FROM categorie WHERE titre = :titre");
		$verif_categorie->bindValue(':titre', strtolower($_POST['titre']));
		$resultat = $verif_categorie->execute();
		if($verif_categorie->rowCount() > 0)
		{
			$content .= '<div class="alert alert-danger col-md-offset-2 text-center">Cette catégorie existe déjà ;-(</div>';
		} else {
			//echo $_POST['id_categorie'];
			// Réaliser le traitement de l'insertion de la photo
			$donnees = $pdo->prepare("INSERT INTO categorie (titre, motscles) VALUES(:titre, :motscles)");
			$donnees->bindValue(':titre', strtolower($_POST['titre']));
			$donnees->bindValue(':motscles', strtolower($_POST['motscles']));
			$donnees->execute();
			$_GET['action'] = 'affichage';
			//debug($donnees);
			$content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">Votre annonce ayant le titre <strong class="text-success"> ' . $_POST['titre'] . '</strong> a bien été ajouté à la boutique</div>';
		}
    }
    else
    {
        $verif_categorie = $pdo->prepare("SELECT * FROM categorie WHERE titre = :titre");
		$verif_categorie->bindValue(':titre', strtolower($_POST['titre']));
		$resultat = $verif_categorie->execute();
		if($verif_categorie->rowCount() > 0)
		{
			$content .= '<div class="alert alert-danger col-md-offset-2 text-center">Cette catégorie existe déjà ;-(</div>';
		} else {
			// Requete de modification
			$donnees = $pdo->prepare("UPDATE categorie SET titre = :titre, motscles = :motscles WHERE id_categorie = $_POST[id_categorie]");
			$donnees->bindValue(':titre', strtolower($_POST['titre']));
			$donnees->bindValue(':motscles', strtolower($_POST['motscles']));
			$donnees->execute();
			$_GET['action'] = 'affichage';
			
			$content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">Votre annonce ayant le titre <strong class="text-success"> ' . $_POST['titre'] . '</strong> a bien modifié!!</div>';
		}
    }
    
    

}

//---- LIENS annonceS
$content .= '<div class="list-group col-md-6 col-md-offset-3">';
$content .= '<h3 class="list-group-item active text-center">BACKEND</h3>';
$content .= '<a href="?action=affichage" class="list-group-item text-center">Affichez les catégories</a>';
$content .= '<a href="?action=ajout" class="list-group-item text-center">Ajouter une catégorie</a>';
$content .= '<hr></div>';
//$_GET['action'] = 'affichage';
//---- AFFICHAGE annonceS
if(isset($_GET['action']) && $_GET['action'] == 'affichage')
{
    $resultat = $pdo->query("SELECT * FROM categorie");
    $content .= '<div class="col-md-10 col-md-offset-1 text-center"><h3 class="text-center">Affichage annonces</h3>';
    
    $content .= 'Nombre de annonce(s) dans la boutique <span class="badge">' . $resultat->rowCount() . '</span></div>';
    
    $content .= '<table class="col-md-10 table" style="margin-top: 10px;"><tr class="active">';
	$content .= '<th>Id de la catégorie</th><th>Titre</th><th>Mots clés</th><th>Modification</th><th>Supprimer</th></tr>';
    
    while($annonce =  $resultat->fetch(PDO::FETCH_ASSOC))
    {
        $content .= '<tr>';
		$content .= '<td>'.$annonce['id_categorie'].'</td>';
		$content .= '<td>'.ucfirst($annonce['titre']).'</td>';
		$content .= '<td>'.$annonce['motscles'].'</td>';
        $content .= '<td class="text-center"><a href="?action=modification&id_categorie=' . $annonce['id_categorie'] . '"><span class="glyphicon glyphicon-pencil"></span></a></td>';
		$content .= '<td class="text-center"><a href="?action=suppression&id_categorie=' . $annonce['id_categorie'] . '" onClick="return(confirm(\'En êtes vous certain ?\'));"><span class="glyphicon glyphicon-trash"></span></a></td>';
        $content .= '</tr>';
    }
    $content .= '</table>';
    
    
    
}

require_once("../inc/header.inc.php");
echo $content;

//debug($_POST);
//debug($_FILES);

if(isset($_GET['action']) && ($_GET['action'] == 'ajout' || $_GET['action'] == 'modification'))
{
    if(isset($_GET['id_categorie']))
    {
        $resultat = $pdo->prepare("SELECT * FROM categorie WHERE id_categorie = :id_categorie ");
        $resultat->bindValue(':id_categorie', $_GET['id_categorie'], PDO::PARAM_INT);
        $resultat->execute();
        
        $annonce_actuel = $resultat->fetch(PDO::FETCH_ASSOC);
        //debug($annonce_actuel);
        
        foreach($annonce_actuel as $indice => $valeur)
        {
            //debug($indice);
            $$indice = (isset($annonce_actuel["$indice"])) ? $annonce_actuel["$indice"] : ''; 
        }
    }
    else
    {
            $resultat = $pdo->query("SELECT * FROM categorie LIMIT 0,1");
           
            $annonce = $resultat->fetch(PDO::FETCH_ASSOC);
            foreach($annonce as $indice => $valeur)
            {
                $$indice = '';
            }
    }
    
    echo '<form method="post" action="" enctype="multipart/form-data" class="col-md-8 col-md-offset-2">
        <h2 class="text-center">' . ucfirst($_GET['action']) . ' d\'une catégorie</h2>
        
        <input type="hidden" id="id_categorie" name="Id catégorie" value="' . $id_categorie . '">
      <div class="form-group">
        <label for="titre">Titre</label>
        <input type="text" class="form-control" id="titre" name="titre" placeholder="Titre" value="' . ucfirst($titre) . '">
      </div>
      <div class="form-group">
        <label for="description_courte">Mots clés</label>
        <input type="text" class="form-control" id="motscles" name="motscles" placeholder="Mots clés" value="' . $motscles . '">
      </div><button type="submit" class="btn btn-primary col-md-12">' . ucfirst($_GET['action']) . ' d\'une catégorie</button>
    </form>';

}

require_once("../inc/footer.inc.php");