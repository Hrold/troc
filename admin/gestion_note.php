<?php
require_once("../inc/init.inc.php");

if(!internauteEstConnecteEtEstAdmin()) // si l'internaute n'est pas connecté, il n'a rien faire la, on le redirige vers la page connexion
{
    header("location:" . URL . "connexion.php");
}
$id_membre_session = $_SESSION['membre']['id_membre'];
//-------- SUPPRESSION catégorie --------------//
if(isset($_GET['action']) && $_GET['action'] == 'suppression')
{
    // Exercice : requete de suppression catégorie
    $resultat = $pdo->prepare("DELETE FROM note WHERE id_note = :id_note");
    $resultat->bindValue(':id_note', $_GET['id_note'], PDO::PARAM_INT);
    $resultat->execute();
    
    $_GET['action'] = 'affichage';
    
    $content .= '<div class="alert alert-success col-md-8 col-md-offset-2 text-center">La note n° ' . $_GET['id_note'] . ' a bien été supprimée! </div>';
    
}


if(!empty($_POST))
{
    //$erreur='';
    if(isset($_GET['action']) && ($_GET['action'] == 'modification'))
    {
		
			// Requete de modification
			$donnees = $pdo->prepare("UPDATE note SET note = :note, avis = :avis WHERE id_note = $_GET[id_note]");
			$donnees->bindValue(':note', $_POST['note']);
			$donnees->bindValue(':avis', $_POST['avis']);
			$donnees->execute();
			$_GET['action'] = 'affichage';
			
			$content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">La note n°'.$_GET['id_note'].' a bien modifié!!</div>';
		}
    }
    

//---- LIENS annonceS
$content .= '<div class="list-group col-md-6 col-md-offset-3">';
$content .= '<h3 class="list-group-item active text-center">BACKEND</h3>';
$content .= '<a href="?action=affichage" class="list-group-item text-center">Afficher les note</a>';
$content .= '<hr></div>';
//$_GET['action'] = 'affichage';
//---- AFFICHAGE annonceS
if(isset($_GET['action']) && $_GET['action'] == 'affichage')
{
    $resultat = $pdo->query("SELECT * FROM note");
    $content .= '<div class="col-md-10 col-md-offset-1 text-center"><h3 class="text-center">Affichage note</h3>';
    
    $content .= 'Nombre de notes <span class="badge">' . $resultat->rowCount() . '</span></div>';
    
    $content .= '<table class="col-md-10 table" style="margin-top: 10px;"><tr class="active">';
	$content .= '<th>Id note</th><th>M-id1</th><th>M-id2</th><th>Note</th><th>Avis</th><th>Date enregistrement</th><th>Modification</th><th>Supprimer</th></tr>';
    
    while($annonce =  $resultat->fetch(PDO::FETCH_ASSOC))
    {
		
        $content .= '<tr>';
		$content .= '<td>'.$annonce['id_note'].'</td>';
		$content .= '<td>'.$annonce['membre_id1'].'</td>';
		$content .= '<td>'.$annonce['membre_id2'].'</td>';
		$content .= '<td>'.$annonce['note'].'</td>';
		$content .= '<td>'.$annonce['avis'].'</td>';
		$content .= '<td>'.$annonce['date_enregistrement'].'</td>';
        $content .= '<td class="text-center"><a href="?action=modification&id_note=' . $annonce['id_note'] . '"><span class="glyphicon glyphicon-pencil"></span></a></td>';
		$content .= '<td class="text-center"><a href="?action=suppression&id_note=' . $annonce['id_note'] . '" onClick="return(confirm(\'En êtes vous certain ?\'));"><span class="glyphicon glyphicon-trash"></span></a></td>';
        $content .= '</tr>';
    }
    $content .= '</table>';
    
    
    
}

require_once("../inc/header.inc.php");
echo $content;

//debug($_POST);
//debug($_FILES);

if(isset($_GET['action']) && ( $_GET['action'] == 'modification'))
{
    if(isset($_GET['id_note']))
    {
        $resultat = $pdo->prepare("SELECT * FROM note WHERE id_note = :id_note ");
        $resultat->bindValue(':id_note', $_GET['id_note'], PDO::PARAM_INT);
        $resultat->execute();
        
        $annonce_actuel = $resultat->fetch(PDO::FETCH_ASSOC);
        //debug($annonce_actuel);
        
        foreach($annonce_actuel as $indice => $valeur)
        {
           // debug($indice);
            $$indice = (isset($annonce_actuel["$indice"])) ? $annonce_actuel["$indice"] : ''; 
        }
    }
    else
    {
            $resultat = $pdo->query("SELECT * FROM note LIMIT 0,1");
           
            $annonce = $resultat->fetch(PDO::FETCH_ASSOC);
            foreach($annonce as $indice => $valeur)
            {
                $$indice = '';
            }
    }
    
    echo '<form method="post" action="" enctype="multipart/form-data" class="col-md-8 col-md-offset-2">
        <h2 class="text-center">' . ucfirst($_GET['action']) . ' d\'un commentaire</h2>
        <input type="hidden" id="id_note" name="id_note" value="' . $id_note . '">
      <div class="form-group">
	  <label>Membre id 1: '.$membre_id1.'</label>
	  </div>
	  <div class="form-group">
	  <label>Membre id 2: '.$membre_id2.'</label>
	  </div>
	  <div class="form-group">
	  <label for="note" >Note</label>
		<input type ="text" class="form-control"id="note" name="note" value="'.$note.'">
	  </div>
	  <div class="form-group">
        <label for="avis">Avis</label>
        <input type="text" class="form-control" id="avis" name="avis" placeholder="Avis" value="'.$avis.'">
      </div>
      <div class="form-group">
        <label for="date_enregistrement">Date enregistrement : '.$date_enregistrement.'</label>
      </div><button type="submit" class="btn btn-primary col-md-12">' . ucfirst($_GET['action']) . ' d\'un commentaire</button>
    </form>';

}

require_once("../inc/footer.inc.php");