<?php
require_once("../inc/init.inc.php");

if(!internauteEstConnecteEtEstAdmin()) // si l'internaute n'est pas ADMIN, il n'a rien faire la, on le redirige vers la page connexion
{
    header("location:" . URL . "connexion.php");
}
$id_membre_session = $_SESSION['membre']['id_membre'];
//-------- SUPPRESSION annonce --------------//
if(isset($_GET['action']) && $_GET['action'] == 'suppression')
{
    // Exercice : requete de suppression annonce
    $resultat = $pdo->prepare("DELETE FROM annonce WHERE id_annonce = :id_annonce");
    $resultat->bindValue(':id_annonce', $_GET['id_annonce'], PDO::PARAM_INT);
    $resultat->execute();
    
    $_GET['action'] = 'affichage';
    
    $content .= '<div class="alert alert-success col-md-8 col-md-offset-2 text-center">Le annonce n° ' . $_GET['id_annonce'] . ' a bien été supprimé! </div>';
    
}

    $photo_bdd = '';
    if(isset($_GET['action']) && $_GET['action'] == 'modif')
    {
	$erreur = '';
    if(empty($_POST['titre']) || strlen($_POST['titre']) <4)
    {
        $erreur .= '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Veuillez insèrer un titre de plus de 4 caractères</div>';
    }
	if(empty($_POST['description_courte']) || strlen($_POST['description_courte']) <4 || strlen($_POST['description_courte']) > 20)
    {
        $erreur .= '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Veuillez insèrer une description courte de plus de 4 caractères ou de moins de 20 caractères</div>';
    }
	if(empty($_POST['description_longue']) || strlen($_POST['description_longue']) <20)
    {
        $erreur .= '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Un peu d\'inspiration pour la description longue, un effort, il faut au  moins 20 caractères</div>';
    }
	if(empty($_POST['prix']) || !is_numeric($_POST['prix']))
    {
        $erreur .= '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Veuillez insèrer un prix</div>';
    }
	if(empty($_POST['adresse']))
    {
        $erreur .= '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Entrez une adresse</div>';
    }
	if(empty($_POST['pays']))
    {
        $erreur .= '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Veuillez renseignez le pays</div>';
    }
	if(empty($_POST['ville']))
    {
        $erreur .= '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Veuillez renseignez la ville</div>';
    }
	/* if(!is_int($_POST['cp']) || strlen($_POST['cp'])!=5)
    {
        $erreur .= '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Le code postal doit contenir 5 chiffres</div>';
    } */
        $photo_bdd = $_POST['photo_actuelle'];
    if(!empty($_FILES['photo']['name']))
    {
        $nom_photo = $id_membre_session.'-'.time(). '-' . $_FILES['photo']['name']; 
        
        $photo_bdd = URL."images/$nom_photo";
        //echo $photo_bdd.'<br>';
        
        $photo_dossier = RACINE_SITE."/$nom_photo";
        //echo $photo_dossier;
        
        copy($_FILES['photo']['tmp_name'], $photo_dossier);
    }
	if (empty($erreur)){
		 // Requete de modification
        $donnees = $pdo->prepare("UPDATE annonce SET titre = :titre, description_courte = :description_courte, description_longue = :description_longue, prix = :prix, photo = :photo, pays = :pays, ville = :ville, adresse = :adresse, cp = :cp, categorie_id = :id_categorie  WHERE id_annonce = :id_annonce");
		$donnees->bindValue(':titre', $_POST['titre']);
		$donnees->bindValue(':description_courte', $_POST['description_courte']);
		$donnees->bindValue(':description_longue', $_POST['description_longue']);
		$donnees->bindValue(':prix', $_POST['prix']);
		$donnees->bindValue(':photo', $photo_bdd);
		$donnees->bindValue(':pays', $_POST['pays']);
		$donnees->bindValue(':ville', $_POST['ville']);
		$donnees->bindValue(':adresse', $_POST['adresse']);
		$donnees->bindValue(':cp', $_POST['cp']);
		//$donnees->bindValue(':id_membre', $_POST['id_membre'], PDO::PARAM_INT);
		$donnees->bindValue(':id_categorie', $_POST['id_categorie']);
		$donnees->bindValue(':id_annonce', $_POST['id_annonce']);
        $donnees->execute();
		//$res = $donnees->fetch(PDO::FETCH_ASSOC);
		//var_dump($res);
		
        $_GET['action'] = 'affichage';
        
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">L\'annonce numéro<strong class="text-success"> ' . $_POST['id_annonce'] . '</strong> a bien modifié!!</div>';
	}$_GET['action'] = 'modification';
}
//---- LIENS annonceS
$content .= '<div class="list-group col-md-6 col-md-offset-3">';
$content .= '<h3 class="list-group-item active text-center">BACK OFFICE</h3>';
$content .= '<a href="?action=affichage" class="list-group-item text-center">Affichage annonces</a>';
$content .= '<hr></div>';

//---- AFFICHAGE annonceS
if(isset($_GET['action']) && $_GET['action'] == 'affichage')
{
    $resultat = $pdo->query("SELECT * FROM annonce");
    $content .= '<div class="col-md-10 text-center"><h3 class="text-center">Affichage annonces</h3>';
    
    $content .= 'Nombre de annonce(s) dans la boutique <span class="badge">' . $resultat->rowCount() . '</span></div>';
    
    $content .= '<table class="col-md-10 table" style="margin-top: 10px;"><tr class="active">';
    /* for($i = 0; $i < $resultat->columnCount(); $i++)
    {
        $colonne = $resultat->getColumnMeta($i);
        $content .= '<th>' . $colonne['name'] . '</th>';
    } */
	$content.= '<th>Id</th>';
	$content.= '<th>Titre</th>';
	$content.= '<th>Description</th>'; 
	$content.= '<th>Prix</th>'; 
	$content.= '<th>Photo</th>';
	$content.= '<th>Adresse</th>'; 
	$content.= '<th>Membre</th>';
	$content.= '<th>Catégorie</th>'; 
	$content.= '<th>Date</th>'; 
    $content .= '<th>Modif.</th>';
    $content .= '<th>Suppr.</th>';
    $content .= '</tr>';
    while($annonce =  $resultat->fetch(PDO::FETCH_ASSOC))
    {
		$membre=$annonce['membre_id'];
		$resultat2 = $pdo->query("SELECT pseudo FROM membre WHERE id_membre = $membre");
		$pseudo = $resultat2->fetch(PDO::FETCH_ASSOC);
		//var_dump($resultat2);
		$cat = $annonce['categorie_id'];
		$resultat3 = $pdo->query("SELECT titre FROM categorie WHERE id_categorie = $cat");
		$cat = $resultat3->fetch(PDO::FETCH_ASSOC);
		
        $content .= '<tr>';
		
		$content .= '<td>'.$annonce['id_annonce'].'</td>';
		$content .= '<td>'.$annonce['titre'].'</td>';
		$content .= '<td>'.$annonce['description_courte'].'</td>';
		$content .= '<td>'.$annonce['prix'].' €</td>';
		$content .= '<td><img src=' . $annonce['photo'] . ' width="70" height="70">';
		$content .= '<td>'.$annonce['adresse'].' - '.$annonce['cp'].' - '.$annonce['ville'].'</td>';
		$content .= '<td>'.$pseudo['pseudo'].'</td>';
		$content .= '<td>'.$cat['titre'].'</td>';
		$content .= '<td>'.$annonce['date_enregistrement'].'</td>';
        /* foreach($annonce as $indice => $infos)
        {
            
            if($indice == 'photo')
            {
                $content .= '<td><img src=' . $infos . ' width="70" height="70"></td>'; 
            } elseif($indice == 'photo_id') {}
            else
            {
                $content .= '<td>' . $infos . '</td>'; 
            }
        } */
        $content .= '<td class="text-center"><a href="?action=modification&id_annonce=' . $annonce['id_annonce'] . '"><span class="glyphicon glyphicon-pencil"></span></a></td>';
        
        $content .= '<td class="text-center"><a href="?action=suppression&id_annonce=' . $annonce['id_annonce'] . '" onClick="return(confirm(\'En êtes vous certain ?\'));"><span class="glyphicon glyphicon-trash"></span></a></td>';
        $content .= '</tr>';
    }
    $content .= '</table>';
    
    
    
}

require_once("../inc/header.inc.php");
echo $erreur;
echo $content;
//debug($_POST);
//debug($_FILES);

if(isset($_GET['action']) && ($_GET['action'] == 'modification'))
{
    if(isset($_GET['id_annonce']))
    {
        $resultat = $pdo->prepare("SELECT * FROM annonce WHERE id_annonce = :id_annonce");
        $resultat->bindValue(':id_annonce', $_GET['id_annonce'], PDO::PARAM_INT);
        $resultat->execute();
        
        $annonce_actuel = $resultat->fetch(PDO::FETCH_ASSOC);
        //debug($annonce_actuel);
        
        foreach($annonce_actuel as $indice => $valeur)
        {
            //debug($indice);
            $$indice = (isset($annonce_actuel["$indice"])) ? $annonce_actuel["$indice"] : ''; 
        }
    }
    else
    {
            $resultat = $pdo->query("SELECT * FROM annonce LIMIT 0,1");
           
            $annonce = $resultat->fetch(PDO::FETCH_ASSOC);
            foreach($annonce as $indice => $valeur)
            {
                $$indice = '';
            }
    }
    
    echo '<form method="post" action="?action=modif&id_annonce='.$id_annonce.'" enctype="multipart/form-data" class="col-md-8 col-md-offset-2">
        <h2 class="text-center">' . ucfirst($_GET['action']) . ' de l\' annonce</h2>
        
        <input type="hidden" id="id_annonce" name="id_annonce" value="' . $id_annonce . '">
		<input type="hidden" id="id_membre" name="id_membre" value="' . $membre_id . '">
      <div class="form-group">
        <label for="titre">Titre</label>
        <input type="text" class="form-control" id="titre" name="titre" placeholder="titre" value="' . $titre . '">
      </div>
      <div class="form-group">
        <label for="description_courte">Description courte</label>
        <input type="text" class="form-control" id="description_courte" name="description_courte" placeholder="description_courte" value="' . $description_courte . '">
      </div>
      <div class="form-group">
        <label for="description_longue">description_longue</label>
        <textarea class="form-control" rows="3" id="description_longue" name="description_longue" placeholder="description_longue">' . $description_longue . '</textarea>
      </div>
      <div class="form-group">
        <label for="prix">Prix</label>
        <input type="text"class="form-control" id="prix" name="prix" placeholder="prix" value="'. $prix . '">
      </div>    
   
      <div class="form-group">
        <label for="photo">Photo</label>
        <input type="file" id="photo" name="photo"><br>';
      if(!empty($photo))
      {
          echo '<em>Vous pouvez uploader une nouvelle photo si vous souhaitez la changer</em><br>';
          echo '<img src="' . $photo . '" width="90" height="90">';
      }
      echo '<input type="hidden" id="photo_actuelle" name="photo_actuelle" value="' . $photo . '">';    
      echo '</div>    
      <div class="form-group">
        <label for="pays">Pays</label>
        <input type="text" class="form-control" id="pays" name="pays" placeholder="pays" value="' . $pays . '">
      </div>
      <div class="form-group">
        <label for="ville">Ville</label>
        <input type="text" class="form-control" id="ville" name="ville" placeholder="ville" value="' . $ville . '">
      </div>    
      <div class="form-group">
        <label for="adresse">adresse</label>
        <input type="text" class="form-control" id="adresse" name="adresse" placeholder="adresse" value="' . $adresse . '">
      </div>
      <div class="form-group">
        <label for="cp">Code postal</label>
        <input type="text" class="form-control" id="cp" name="cp" placeholder="cp" value="' . $cp . '">
      </div>  
	   <div class="form-group">
        <label for="id_categorie"> Catégorie</label>';
		$tab_multi = executeRequete("SELECT * FROM categorie");
		
		echo '<select id="id_categorie" name="id_categorie" >';
		
		while ($ensembleC = $tab_multi->fetch(PDO::FETCH_ASSOC)){
			echo '<option value="'.$ensembleC['id_categorie'].'">'.$ensembleC['titre'].'</option>';
		}
		echo '</select>';
		
      echo'<button type="submit" class="btn btn-primary col-md-12">' . ucfirst($_GET['action']) . ' du annonce</button>
    </form></div>';
}


require_once("../inc/footer.inc.php");