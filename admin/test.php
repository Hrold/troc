<?php
require_once("../inc/init.inc.php");

if(!internauteEstConnecteEtEstAdmin()) // si l'internaute n'est pas ADMIN, il n'a rien faire la, on le redirige vers la page connexion
{
    header("location:" . URL . "connexion.php");
}

//-------- SUPPRESSION annonce --------------//
if(isset($_GET['action']) && $_GET['action'] == 'suppression')
{
    // Exercice : requete de suppression annonce
    $resultat = $pdo->prepare("DELETE FROM annonce WHERE id_annonce = :id_annonce");
    $resultat->bindValue(':id_annonce', $_GET['id_annonce'], PDO::PARAM_INT);
    $resultat->execute();
    
    $_GET['action'] = 'affichage';
    
    $content .= '<div class="alert alert-success col-md-8 col-md-offset-2 text-center">Le annonce n° ' . $_GET['id_annonce'] . ' a bien été supprimé! </div>';
    
}


if(!empty($_POST))
{
    $photo_bdd = '';
    if(isset($_GET['action']) && $_GET['action'] == 'modification')
    {
        $photo_bdd = $_POST['photo_actuelle'];
    }
    if(!empty($_FILES['photo']['name']))
    {
        $nom_photo = $_POST['reference'] . '-' . $_FILES['photo']['name']; 
        
        $photo_bdd = URL . "images/$nom_photo";
        //echo $photo_bdd;
        
        $photo_dossier = RACINE_SITE . "images/$nom_photo";
        //echo $photo_dossier;
        
        copy($_FILES['photo']['tmp_name'], $photo_dossier);
    }
    
    if(isset($_GET['action']) && ($_GET['action'] == 'ajout'))
    {
			echo "AZFEZAGRAZGRAZGZ";
        // Réaliser le traitement de l'insertion de la photo
        $donnees = $pdo->prepare("INSERT INTO annonce (titre, description_courte, description_longue, prix, photo, pays, ville, adresse, cp, membre_id, categorie_id, date_enregistrement) VALUES(:titre, :description_courte, :description_longue, :prix, :photo, :pays, :ville, :adresse, :cp, :membre_id, :categorie_id, NOW())");
		$donnees->bindValue(':titre', $_POST['titre']);
		$donnees->bindValue(':description_courte', $_POST['description_courte']);
		$donnees->bindValue(':description_longue', $_POST['description_longue']);
		$donnees->bindValue(':prix', $_POST['prix'], PDO::PARAM_INT);
		$donnees->bindValue(':photo', $_POST['photo']);
		$donnees->bindValue(':pays', $_POST['pays']);
		$donnees->bindValue(':ville', $_POST['ville']);
		$donnees->bindValue(':adresse', $_POST['adresse']);
		$donnees->bindValue(':cp', $_POST['cp'], PDO::PARAM_INT);
		$donnees->bindValue(':membre_id', $_POST['membre_id'], PDO::PARAM_INT);
		$donnees->bindValue(':categorie_id', $_POST['categorie_id'], PDO::PARAM_INT);
        $donnees->execute();
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">Le annonce reference <strong class="text-success"> ' . $_POST['reference'] . '</strong> a bien été ajouté à la boutique</div>';
    }
    else
    {
        // Requete de modification
        $donnees = $pdo->prepare("UPDATE annonce SET titre = :titre, description_courte = :description_courte, description_longue = :description_longue, prix = :prix, photo = :photo, pays = :pays, ville = :ville, adresse = :adresse, cp = :cp, membre_id = :membre_id, categorie_id = :categorie_id  WHERE id_annonce = $_POST[id_annonce]");
        $donnees->execute();
        $_GET['action'] = 'affichage';
        
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">Le annonce reference <strong class="text-success"> ' . $_POST['reference'] . '</strong> a bien modifié!!</div>';
    }
    
    
    $resultat =  $pdo->query("SELECT * FROM annonce");
    
    for($i = 0; $i < $resultat->columnCount(); $i++)
    {
        $colonne = $resultat->getColumnMeta($i);
        //debug($colonne);
        if($colonne['name'] != 'id_annonce')
        {
            if($colonne['name'] == 'photo')
            {
                $donnees->bindValue(":$colonne[name]", $photo_bdd, PDO::PARAM_STR);
            }
            else
            {
                $donnees->bindValue(":$colonne[name]", $_POST["$colonne[name]"], PDO::PARAM_STR);
            }
        }    
    }
    
    $donnees->execute(); 
    
}

//---- LIENS annonceS
$content .= '<div class="list-group col-md-6 col-md-offset-3">';
$content .= '<h3 class="list-group-item active text-center">BACK OFFICE</h3>';
$content .= '<a href="?action=affichage" class="list-group-item text-center">Affichage annonces</a>';
$content .= '<a href="?action=ajout" class="list-group-item text-center">Ajout annonce</a>';
$content .= '<hr></div>';

//---- AFFICHAGE annonceS
if(isset($_GET['action']) && $_GET['action'] == 'affichage')
{
    $resultat = $pdo->query("SELECT * FROM annonce");
    $content .= '<div class="col-md-10 col-md-offset-1 text-center"><h3 class="text-center">Affichage annonces</h3>';
    
    $content .= 'Nombre de annonce(s) dans la boutique <span class="badge">' . $resultat->rowCount() . '</span></div>';
    
    $content .= '<table class="col-md-10 table" style="margin-top: 10px;"><tr class="active">';
    for($i = 0; $i < $resultat->columnCount(); $i++)
    {
        $colonne = $resultat->getColumnMeta($i);
        $content .= '<th>' . $colonne['name'] . '</th>';
    }
    $content .= '<th>Modification</th>';
    $content .= '<th>Suppression</th>';
    $content .= '</tr>';
    while($annonce =  $resultat->fetch(PDO::FETCH_ASSOC))
    {
        $content .= '<tr>';
        foreach($annonce as $indice => $infos)
        {
            
            if($indice == 'photo')
            {
                $content .= '<td><img src="' . $infos . '" width="70" height="70"></td>'; 
            }
            else
            {
                $content .= '<td>' . $infos . '</td>'; 
            }
        }
        $content .= '<td class="text-center"><a href="?action=modification&id_annonce=' . $annonce['id_annonce'] . '"><span class="glyphicon glyphicon-pencil"></span></a></td>';
        
        $content .= '<td class="text-center"><a href="?action=suppression&id_annonce=' . $annonce['id_annonce'] . '" onClick="return(confirm(\'En êtes certain ?\'));"><span class="glyphicon glyphicon-trash"></span></a></td>';
        $content .= '</tr>';
    }
    $content .= '</table>';
    
    
    
}

require_once("../inc/header.inc.php");
echo $content;
//debug($_POST);
//debug($_FILES);

if(isset($_GET['action']) && ($_GET['action'] == 'ajout' || $_GET['action'] == 'modification'))
{
    if(isset($_GET['id_annonce']))
    {
        $resultat = $pdo->prepare("SELECT * FROM annonce WHERE id_annonce = :id_annonce");
        $resultat->bindValue(':id_annonce', $_GET['id_annonce'], PDO::PARAM_INT);
        $resultat->execute();
        
        $annonce_actuel = $resultat->fetch(PDO::FETCH_ASSOC);
        //debug($annonce_actuel);
        
        foreach($annonce_actuel as $indice => $valeur)
        {
            //debug($indice);
            $$indice = (isset($annonce_actuel["$indice"])) ? $annonce_actuel["$indice"] : ''; 
        }
    }
    else
    {
            $resultat = $pdo->query("SELECT * FROM annonce LIMIT 0,1");
           
            $annonce = $resultat->fetch(PDO::FETCH_ASSOC);
            foreach($annonce as $indice => $valeur)
            {
                $$indice = '';
            }
    }
    
    echo '<form method="post" action="" enctype="multipart/form-data" class="col-md-8 col-md-offset-2">
        <h2 class="text-center">' . ucfirst($_GET['action']) . ' du annonce</h2>
        
        <input type="hidden" id="id_annonce" name="id_annonce" value="' . $id_annonce . '">
      <div class="form-group">
        <label for="titre">Titre</label>
        <input type="text" class="form-control" id="titre" name="titre" placeholder="titre" value="' . $titre . '">
      </div>
      <div class="form-group">
        <label for="description_courte">Description courte"</label>
        <input type="text" class="form-control" id="description_courte" name="description_courte" placeholder="description_courte" value="' . $description_courte . '">
      </div>
      <div class="form-group">
        <label for="description_longue">description_longue</label>
        <textarea class="form-control" rows="3" id="description_longue" name="description_longue" placeholder="description_longue">' . $description_longue . '</textarea>
      </div>
      <div class="form-group">
        <label for="prix">Prix</label>
        <input type="text"class="form-control" id="prix" name="prix" placeholder="prix" value="'. $prix . '">
      </div>    
   
      <div class="form-group">
        <label for="photo">Photo</label>
        <input type="file" id="photo" name="photo"><br>';
      if(!empty($photo))
      {
          echo '<em>Vous pouvez uploader une nouvelle photo si vous souhaitez la changer</em><br>';
          echo '<img src="' . $photo . '" width="90" height="90">';
      }
      echo '<input type="hidden" id="photo_actuelle" name="photo_actuelle" value="' . $photo . '">';    
      echo '</div>    
      <div class="form-group">
        <label for="pays">Pays</label>
        <input type="text" class="form-control" id="pays" name="pays" placeholder="pays" value="' . $pays . '">
      </div>
      <div class="form-group">
        <label for="ville">Ville</label>
        <input type="text" class="form-control" id="ville" name="ville" placeholder="ville" value="' . $ville . '">
      </div> 
	</div>    
      <div class="form-group">
        <label for="adresse">adresse</label>
        <input type="text" class="form-control" id="adresse" name="adresse" placeholder="adresse" value="' . $adresse . '">
      </div>
      <div class="form-group">
        <label for="cp">Code postal</label>
        <input type="text" class="form-control" id="cp" name="cp" placeholder="cp" value="' . $cp . '">
      </div>  
	   <div class="form-group">
        <label for="id_categorie"> Catégorie</label>';
		$tab_multi = executeRequete("SELECT * FROM categorie");
		echo '<select id="id_categorie" name="id_categorie">';
		while ($ensembleC = $tab_multi->fetch(PDO::FETCH_ASSOC)){
	
		echo '<option>'.$ensembleC['titre'].'</option>';
		}
		echo '</select>';
      echo'<button type="submit" class="btn btn-primary col-md-12">' . ucfirst($_GET['action']) . ' du annonce</button>
    </form>';
}


require_once("../inc/footer.inc.php");