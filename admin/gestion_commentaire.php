<?php
require_once("../inc/init.inc.php");

if(!internauteEstConnecteEtEstAdmin()) // si l'internaute n'est pas connecté, il n'a rien faire la, on le redirige vers la page connexion
{
    header("location:" . URL . "connexion.php");
}
$id_membre_session = $_SESSION['membre']['id_membre'];
//-------- SUPPRESSION catégorie --------------//
if(isset($_GET['action']) && $_GET['action'] == 'suppression')
{
    // Exercice : requete de suppression catégorie
    $resultat = $pdo->prepare("DELETE FROM commentaire WHERE id_commentaire = :id_commentaire");
    $resultat->bindValue(':id_commentaire', $_GET['id_commentaire'], PDO::PARAM_INT);
    $resultat->execute();
    
    $_GET['action'] = 'affichage';
    
    $content .= '<div class="alert alert-success col-md-8 col-md-offset-2 text-center">Le commentaire n° ' . $_GET['id_commentaire'] . ' a bien été supprimé! </div>';
    
}


if(!empty($_POST))
{
    $erreur='';
    if(isset($_GET['action']) && ($_GET['action'] == 'modification'))
    {
		if (empty($_POST['commentaire']) || strlen($_POST['commentaire'])<10){
			$erreur .=  '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Veuillez insèrer un commentaire supérieur à 10 caractères</div>';
			
		} else {
				// Requete de modification
				$donnees = $pdo->prepare("UPDATE commentaire SET commentaire = :commentaire WHERE id_commentaire = $_GET[id_commentaire]");
				$donnees->bindValue(':commentaire', $_POST['commentaire']);
				$donnees->execute();
				$_GET['action'] = 'affichage';
				
				$content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">Le commentaire n°'.$_GET['id_commentaire'].' a bien modifié!!</div>';
			}
		}
    }
    

//---- LIENS annonceS
$content .= '<div class="list-group col-md-6 col-md-offset-3">';
$content .= '<h3 class="list-group-item active text-center">BACKEND</h3>';
$content .= '<a href="?action=affichage" class="list-group-item text-center">Afficher les commentaires</a>';
$content .= '<hr></div>';
//$_GET['action'] = 'affichage';
//---- AFFICHAGE annonceS
if(isset($_GET['action']) && $_GET['action'] == 'affichage')
{
    $resultat = $pdo->query("SELECT * FROM commentaire");
    $content .= '<div class="col-md-10 col-md-offset-1 text-center"><h3 class="text-center">Affichage annonces</h3>';
    
    $content .= 'Nombre de commentaires <span class="badge">' . $resultat->rowCount() . '</span></div>';
    
    $content .= '<table class="col-md-10 table" style="margin-top: 10px;"><tr class="active">';
	$content .= '<th>Id commentaire</th><th>Membre id</th><th>Annonce id</th><th>Commentaire</th><th>Date enregistrement</th><th>Modification</th><th>Supprimer</th></tr>';
    
    while($annonce =  $resultat->fetch(PDO::FETCH_ASSOC))
    {
		
        $content .= '<tr>';
		$content .= '<td>'.$annonce['id_commentaire'].'</td>';
		$content .= '<td>'.$annonce['membre_id'].'</td>';
		$content .= '<td>'.$annonce['annonce_id'].'</td>';
		$content .= '<td>'.$annonce['commentaire'].'</td>';
		$content .= '<td>'.$annonce['date_enregistrement'].'</td>';
        $content .= '<td class="text-center"><a href="?action=modification&id_commentaire=' . $annonce['id_commentaire'] . '"><span class="glyphicon glyphicon-pencil"></span></a></td>';
		$content .= '<td class="text-center"><a href="?action=suppression&id_commentaire=' . $annonce['id_commentaire'] . '" onClick="return(confirm(\'En êtes vous certain ?\'));"><span class="glyphicon glyphicon-trash"></span></a></td>';
        $content .= '</tr>';
    }
    $content .= '</table>';
    
    
    
}

require_once("../inc/header.inc.php");
echo $erreur;
echo $content;

//debug($_POST);
//debug($_FILES);

if(isset($_GET['action']) && ( $_GET['action'] == 'modification'))
{
    if(isset($_GET['id_commentaire']))
    {
        $resultat = $pdo->prepare("SELECT * FROM commentaire WHERE id_commentaire = :id_commentaire ");
        $resultat->bindValue(':id_commentaire', $_GET['id_commentaire'], PDO::PARAM_INT);
        $resultat->execute();
        
        $annonce_actuel = $resultat->fetch(PDO::FETCH_ASSOC);
        //debug($annonce_actuel);
        
        foreach($annonce_actuel as $indice => $valeur)
        {
           // debug($indice);
            $$indice = (isset($annonce_actuel["$indice"])) ? $annonce_actuel["$indice"] : ''; 
        }
    }
    else
    {
            $resultat = $pdo->query("SELECT * FROM commentaire LIMIT 0,1");
           
            $annonce = $resultat->fetch(PDO::FETCH_ASSOC);
            foreach($annonce as $indice => $valeur)
            {
                $$indice = '';
            }
    }
    
    echo '<form method="post" action="" enctype="multipart/form-data" class="col-md-8 col-md-offset-2">
        <h2 class="text-center">' . ucfirst($_GET['action']) . ' d\'un commentaire</h2>
        <input type="hidden" id="id_commentaire" name="Id commentaire" value="' . $id_commentaire . '">
      <div class="form-group">
	  <label>Membre id : '.$membre_id.'</label>
	  </div>
	  <div class="form-group">
	  <label>Annonce id : '.$annonce_id.'</label>

	  </div>
	  <div class="form-group">
        <label for="commentaire">Commentaire</label>
        <input type="text" class="form-control" id="commentaire" name="commentaire" placeholder="Commentaire" value="'.$commentaire.'">
      </div>
      <div class="form-group">
        <label for="date_enregistrement">Date enregistrement : '.$date_enregistrement.'</label>
      </div><button type="submit" class="btn btn-primary col-md-12">' . ucfirst($_GET['action']) . ' d\'un commentaire</button>
    </form>';

}

require_once("../inc/footer.inc.php");