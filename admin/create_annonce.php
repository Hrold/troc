<?php

require_once('../inc/init.inc.php');

//1- cas du visiteur non connecté :
/* if(!connectUser()){
	header('location:connexion.php'); //on redirige le visiteur vers la page de connexion
	exit();
} */

//require_once('../inc/inc.haut.php');

$valSession = $_SESSION['membre']['id_membre'];

$valSessiontime = $_SESSION['membre']['date_enregistrement'];
//$valCategorie = executeRequete("SELECT categorie_id_categorie FROM annonce WHERE membre_id_membre = $valSession"); 

$exeDate = executeRequete("SELECT NOW()");
$ensembleZ = $exeDate->fetch(PDO::FETCH_ASSOC);
//debug($ensembleZ);


if (!empty($_POST)){ //si le formulaire est soumis
	debug($_POST);
	
	// ici il faudrait mettre tous les controles sur le formulaire .....
	
	$photo_bdd ='';// contiendra le chemin de la photo à insérer en BDD
	
	//9- fin du traitement de la photo :
	if(isset($_POST['photo_actuelle'])){
		//si cela existe c'est que nous sommes en train de modifier la photo: on la remet donc en BDD pour ne pas l'effacer:
		$photo_bdd = $_POST['photo_actuelle'];
	}
	
	//5-traitement de la photo
	debug($_FILES);
	
	if(!empty($_FILES['photo']['name'])){// si une photo est uploadée, la vaveur "name" n'est pas vide
	
		//on crée une varible pour le nom du fichier photo:
		$nom_photo = $_POST['titre'].'_'.$_FILES['photo']['name'];//on crée un nom de fichier unique par référence de produit 
		
		//on crée une variable pour le chemin de la photo à mettre en BDD:
		$photo_bdd ='photo/'.$nom_photo;// chemin relatif de la photo qui est enregistré en DBB necessaire aux balise <img> : photo/monfichier.jpg
		
		//on crée une variable pour le chemin absolue de la photo physique enregistrée sur notre serveur:
		$photo_physique = $_SERVER['DOCUMENT_ROOT'].RACINE_SITE.$photo_bdd; //chemein absolu complet depuis la racine du serveur pour enregistrer le fichier phsysique: c:/wamp64/www/PHP/08-site/photo/nomfichier.jpg
		
		//$_SERVER['DOCUMENT_ROOT'] est une superglobale qui fournit ici la racine du serveur sur lequel  se trouve le site 
		copy($_FILES['photo']['tmp_name'],$photo_physique); //copie le fichier qui est temporairement dans $_FILES['photo']['tmp_name'] vers l'emplacement $photo_physique
		
	}
	
	//enregistrment du produit:
	executeRequete("REPLACE INTO annonce (id_annonce,titre,description_courte,description_longue,prix,photo,pays,ville,adresse,cp,membre_id_membre,categorie_id_categorie,date_enregistrement)
					VALUES(:id_annonce,:titre,:description_courte,:description_longue,:prix,:photo_bdd,:pays,:ville,:adresse,:cp,:membre_id_membre,:categorie_id_categorie,(NOW()))",
					array( ':id_annonce'=> $_POST['id_annonce'],
							':titre'=> $_POST['titre'],
							':description_courte'=> $_POST['description_courte'],
							':description_longue'=> $_POST['description_longue'],
							':prix'=> $_POST['prix'],
							':photo_bdd'=> $photo_bdd,
							':pays'=> $_POST['pays'],
							':ville'=> $_POST['ville'],
							':adresse'=> $_POST['adresse'],
							':cp'=> $_POST['cp'],
							':membre_id_membre'=> $valSession,
							':categorie_id_categorie'=> $_POST['categorie_id_categorie']
							
						));
	//Note : bien mettre les champs dans le même odre que la table "produit" de la BDD car on n'a pas spécifié les champs concernés dans une premier paire de ()
	
	$contenu .='<div class="bg-success">Le produit a bien été enregistré.</div>';
	
	$_GET['action'] ='affichage'; // pour déclencher l'affichage de la table HTML avec tous les produits (cf chapitre 6 ci-desous)
	
}// fin du if (!empty[$_POST)
	
//6 - affichage des produits dans une table HTML:
if(isset($_GET['action']) && $_GET['action'] == 'affichage'){
	// si on a demandé l'affichage de la table HTML:
	
	$resultat = executeRequete("SELECT id_annonce,titre,description_courte,description_longue,photo,prix,pays,ville,adresse,cp FROM annonce WHERE membre_id_membre = $valSession");// sélectionne tous les produits
	
	$contenu .='Nombre d annonce  : ' . $resultat->rowCount();
	
	$contenu .='<table class="table">';
		//affichage des entêtes du tableau
		$contenu .='<tr>';
			//$contenu .='<th>id_annonce</th>';
			$contenu .='<th>titre</th>';
			$contenu .='<th>description_courte</th>';
			$contenu .='<th>description_longue</th>';
			$contenu .='<th>photo</th>';
			$contenu .='<th>prix</th>';
			$contenu .='<th>pays</th>';
			$contenu .='<th>ville</th>';
			$contenu .='<th>adresse</th>';
			$contenu .='<th>cp</th>';
				
			$contenu .='<th>action</th>';
		
		$contenu .='<tr>';
		
		//Affiche des lignes du tableau:
		while($produit = $resultat->fetch(PDO::FETCH_ASSOC)){
			$contenu .='<tr>';
				//on parcourt les informations du tableau associatif $produit:
				foreach($produit as $indice => $information){
					if ($indice == 'photo'){ // pour la photo on met une balise img:
						$contenu .= '<td><img src="../'.$information .'" width="90" height="90"></td>';
					}elseif($indice == 'id_annonce'){
						
					}else{
						//pour les autres champs :
						$contenu .='<td>'. $information .'</td>';
						
					}
				}
			$contenu .='<td>
							<a href="?action=modification&id_annonce='.$produit['id_annonce'] .'">modifier</a>
							-
							<a href="?action=suppression&id_annonce='.$produit['id_annonce'] .'" onclick="return(confirm(\'Etes-vous sûr de vouloir supprimer cette article ? \'));">supprimer</a>
						</td>';

			
			$contenu .='<tr>';
		}
	
	
	$contenu .='</table>';
	
} //fin if(isset($_GET['action']) && $_GET['action'] == 'affichage')


//----------------AFFICHAGE-----------------------
require_once('../inc/header.inc.php');

//2- Onglets 'ajout' et 'affichage des produits':
echo	'<ul class="nav nav-tabs">
			<li><a href="?action=affichage">Afichage des annonces</a></li>
			<li><a href="?action=ajout">Ajouter une annonce</a></li>
		</ul>';
	
echo $contenu; //Pour afficher les messages

//3- Formulaire HTML de produit:
if(isset($_GET['action']) && ($_GET['action'] =='ajout' || $_GET['action'] =='modification')): // syntaxe du if avec ":" et endif à la fin qui remplacent les accolades
// si je suis en ajout ou en modification de produit , j'affiche le formulaire 

	//8- Formulaire de modification avec présaisie des valeurs:
	if(isset($_GET['id_annonce'])){
		//si existe l'indice id_produit , je peux séléctionner le produit en BDD
		$resultat = executeRequete("SELECT * FROM annonce WHERE id_annonce = :id_annonce", array(':id_annonce'=>$_GET['id_annonce']));

		$annonce_actuel = $resultat->fetch(PDO::FETCH_ASSOC); //pas de boucle sur ce fetch car il n'y a qu'un seul produit par id. $produit_actuel est un array qui contient toutes les infos du produit à mettre dans le formulaire		
	}

?>	
	<h3>Formulaire de produit</h3>
	<form method="post" action="" enctype="multipart/form-data"><!--multipart/form-data spécifie que le formulaire envoie des données binaire (=photo) et du texte (=champs du form) : permet d'uploader une photo  -->
	
		<input type="hidden" id="id_annonce" name="id_annonce" value="<?php echo $annonce_actuel['id_annonce'] ?? 0; ?>"> <!-- nécessaire pour la phase de modification d'un produit .Champ caché pour ne pas pouvoir être modifiable -->
		
		<label for="titre">Titre</label><br>
		<input type="text" id="titre" name="titre" value="<?php echo $annonce_actuel['titre'] ?? ''; ?>"><br><br>
		
		<label for="description_courte">Description Courte</label><br>
		<input type="text" id="description_courte" name="description_courte" value="<?php echo $annonce_actuel['description_courte'] ?? ''; ?>"><br><br>
		
		<label for="description_longue">Description longue</label><br>
		<input type="text" id="description_longue" name="description_longue" value="<?php echo $annonce_actuel['description_longue'] ?? ''; ?>"><br><br>
		
				
		<label for="prix">Prix</label><br>
		<input type="text" id="prix" name="prix" value="<?php echo $annonce_actuel['prix'] ?? ''; ?>"><br><br>
		
		<label for="photo">Photo</label><br>
		<!--- 5 upload de la photo -->
		<input type="file" id="photo" name="photo"><!-- ne pas oublier l'attribut enctype ="multipart/form-data" dans la balise form -->
		<!-- 9 modification de la photo -->
		<?php
		if(isset($annonce_actuel['photo'])) { // si existe alors on est en train de modifier un produit: on affiche alors la vignette de la photo actuelle
			echo '<i>Vous pouvez uploader une nouvelle photo</i>';
			echo '<p>Photo actuelle : </p>';
			echo '<img src="../'.$annonce_actuel['photo'].'" name="photo_actuelle" width="90" height="90"><br>';
			echo '<input type="hidden" name="photo_actuelle" value="'.$annonce_actuel['photo'].'"><br>';// on besoin de mettre dans le formulaire le chemin de la photo qui vient de la BDD. Cet input complète le $_POST['photo_actuelle'] qui va en base : on prend la photo de la BDD , puis on la met dans le <form>, puis elle va dans le $_POST puis on la remet dans la BDD
			
		}
		
		?>
		<br>
		<label for="pays">Pays</label><br>
		<input type="text" id="pays" name="pays" value="<?php echo $annonce_actuel['pays'] ?? ''; ?>"><br><br>
				
		<label for="ville">Ville</label><br>
		<input type="text" id="ville" name="ville" value="<?php echo $annonce_actuel['ville'] ?? ''; ?>"><br><br>
		
		
		<label for="adresse">Adresse</label><br>
		<input type="text" id="adresse" name="adresse" value="<?php echo $annonce_actuel['adresse'] ?? ''; ?>"><br><br>
		
		<label for="cp">CP</label><br>
		<input type="text" id="cp" name="cp" value="<?php echo $annonce_actuel['cp'] ?? 0; ?>"><br><br>
		
		<label for="categorie_id_categorie">Categorie</label><br>
		<?php 
		$tab_multi = executeRequete("SELECT * FROM categorie");
		echo '<select id="categorie_id_categorie" name="categorie_id_categorie">';
		while ($ensembleC = $tab_multi->fetch(PDO::FETCH_ASSOC)){
	
		echo '<option>'.$ensembleC['id_categorie'].'</option>';
		}
		echo '</select>';
		
		?><br><br>
		
		<input type="hidden" id="date_enregistrement" name="date_enregistrement" value="<?php echo $annonce_actuel['date_enregistrement'] ?? 0; ?>">
		
		
		
		<input type="submit" value="valider" class="btn">
	
	</form>










<?php

endif;

require_once('../inc/footer.inc.php');