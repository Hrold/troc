<?php
require_once("../inc/init.inc.php");

// 1- Vérification si Admin :
if(!internauteEstConnecteEtEstAdmin())
{
	header("location:../connexion.php");
	exit();
}

$id_membre_session = $_SESSION['membre']['id_membre'];
$pseudo_membre_session = $_SESSION['membre']['pseudo'];
$contenu = '';
$erreur='';

// 3- Suppression d'un membre :
if(isset($_GET['action']) && $_GET['action'] == "supprimer_membre" && isset($_GET['id_membre']))
{	// on ne peut pas supprimer son propre profil :
	if ($_SESSION['membre']['id_membre'] != $_GET['id_membre']) {
		executeRequete("DELETE FROM membre WHERE id_membre=:id_membre", array(':id_membre' => $_GET['id_membre']));
	} else {
		$contenu .= '<div class="bg-danger">Vous ne pouvez pas supprimer votre propre profil ! </div>';
	}
	
}

// 4- Modification statut membre :
if(isset($_GET['action']) && $_GET['action'] == "modifier_statut" && isset($_GET['id_membre']) && isset($_GET['statut']))
{
	if ($_GET['id_membre'] != $_SESSION['membre']['id_membre']) {
		$statut = ($_GET['statut'] == 0) ? 1 : 0;	// si statut = 0 alors il devient 1 sinon devient 0
		//debug($statut);
		executeRequete("UPDATE membre SET statut = '$statut' WHERE id_membre=:id_membre", array(':id_membre' => $_GET['id_membre']));
	} else {
		$contenu .= '<div class="bg-danger">Vous ne pouvez pas modifier votre propre profil ! </div>';	
	}
}

//Modification des infos d'un membre avec mdp
if (isset($_GET['action'])&&($_GET['action']=='modifier'))
    {
    $verif_pseudo = $pdo->prepare("SELECT * FROM membre WHERE pseudo = :pseudo");
    $verif_pseudo->bindValue(':pseudo', $_POST['pseudo']);
    $resultat = $verif_pseudo->execute();
    if($verif_pseudo->rowCount() > 1 && ($_POST['pseudo']!==$pseudo_membre_session))
    {
        $erreur
		.= '<div class="alert alert-danger-8 col-md-offset-2 text-center">Pseudo indisponible!!</div>';
    }
    //------------------------------------------------------
    $verif_email = $pdo->prepare("SELECT * FROM membre WHERE email = :email");
     $verif_email->bindValue(':email', $_POST['email']);
    $verif_email->execute();
    if($verif_email->rowCount() > 1 &&($_POST['email']!==$pseudo_membre_email) )
     {
          $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Ce email existe dèjà dans notre base !</div>';
     }
    //------------------------------------------------------
     if(strlen($_POST['pseudo']) < 4 || strlen($_POST['pseudo']) > 20)
     {
          $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille de pseudo non valide, doit contenir entre 4 et 20 caractères!</div>';
     }
    //------------------------------------------------------
	/* if((strlen($_POST['mdp']) > 0 && strlen($_POST['mdp'])) < 4 || strlen($_POST['mdp']) > 20)
     {
          $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille de mot de passe non valide, doit contenir entre 4 et 20 caractères!</div>';
    } */
    //------------------------------------------------------
    if(!is_numeric($_POST['telephone']) || strlen($_POST['telephone']) !== 10)
    {
          $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille ou format téléphone non valide !</div>';
     }
    //------------------------------------------------------
    //contrôler que le mdp soit confirmé        
    /* if($_POST['mdp'] !== $_POST['mdp_confirm'])
     {
         $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Les deux mots passe ne sont pas identiques.</div>';
     } */
    //------------------------------------------------------
   if(empty($erreur)) // si la variable $erreur est vide, c'est que nous ne sommes pas entreé dans les condotions IF, l'internaute a bien rempli le formulaire, nous pouvons donc executer l'insertion
    {
		//$hash=$resultat->fetch(PDO::FETCH_ASSOC)['mdp'];
       // $mdp = password_hash($_POST['mdp'], PASSWORD_DEFAULT); // password_hash() permet de créer un clé de hachage, on ne garde jamais en clair les mot de passe dans la BDD
        
        
        $donnees = $pdo->prepare("UPDATE membre SET pseudo = :pseudo, nom = :nom, prenom = :prenom, email = :email, civilite = :civilite, telephone = :telephone WHERE id_membre = :id_membre");
        
        $donnees->bindValue(':pseudo', $_POST['pseudo'], PDO::PARAM_STR);
        $donnees->bindValue(':nom', strtolower($_POST['nom']), PDO::PARAM_STR);
        $donnees->bindValue(':prenom', strtolower($_POST['prenom']), PDO::PARAM_STR);
        $donnees->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $donnees->bindValue(':civilite', $_POST['civilite'], PDO::PARAM_STR);
        $donnees->bindValue(':telephone', $_POST['telephone']);
		$donnees->bindValue(':id_membre',$_GET['id_membre']);
        
        $donnees->execute();
        
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">Vos informations ont bien été modifiées <strong class="text-success"> ' . $_POST['pseudo'] . '.';
		//var_dump($donnees);
		//header("location:profil.php");
		if (($_SESSION['membre']['id_membre'] == $_GET['id_membre']) ){
			foreach($donnees as $indice => $valeur)// on passe en revue les données du membre
            {
                if($indice != 'mdp')// on eclu le mdp qui n'est pas conservé dans le fichier session
                {
                    $_SESSION['membre'][$indice] = $valeur; // on créer un tableau membre dans le fichier session et on enregistre les données  

                }
            }
		}
    }
}

if (isset($_GET['action'])&&($_GET['action']=='modifier_mdp'))
    {
		
		 $erreur = '';
    
    
    //------------------------------------------------------
     if(strlen($_POST['mdp']) < 4 || strlen($_POST['mdp']) > 20)
     {
          $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille de mot de passe non valide, doit contenir entre 4 et 20 caractères!</div>';
    }
  
    //contrôler que le mdp soit confirmé        
    if($_POST['mdp'] !== $_POST['mdp_confirm'])
     {
         $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Les deux mots passe ne sont pas identiques.</div>';
     }
    //------------------------------------------------------
	
   if(empty($erreur)) // si la variable $erreur est vide, c'est que nous ne sommes pas entreé dans les condotions IF, l'internaute a bien rempli le formulaire, nous pouvons donc executer l'insertion
    {
		//$hash=$resultat->fetch(PDO::FETCH_ASSOC)['mdp'];
       $mdp = password_hash($_POST['mdp'], PASSWORD_DEFAULT); // password_hash() permet de créer un clé de hachage, on ne garde jamais en clair les mot de passe dans la BDD
        
        
        $donnees = $pdo->prepare("UPDATE membre SET mdp = :mdp WHERE id_membre = :id_membre");
        
        $donnees->bindValue(':mdp', $mdp, PDO::PARAM_STR);
		$donnees->bindValue(':id_membre',$_GET['id_membre']);
		//$resultat->bindValue(':date_enregistrement', NOW());
        
        $donnees->execute();
        
        $content .= '<div class="alert alert-success col-md-6 col-md-offset-3 text-center">Le mot de passe a bien été modifié!!</div>';
    }    
	}

// 2- Préparation de l'affichage :
$resultat = $pdo->prepare("SELECT id_membre, pseudo, nom, prenom, email, civilite, telephone, statut FROM membre");
$resultat->execute();
//var_dump($resultat);

$contenu .= '<h3> Membres inscrit </h3>';
$contenu .=  "Nombre de membre(s) : " . $resultat->rowCount();

$contenu .=  '<table class="table"> <tr>';
		// Affichage des entêtes :
		$contenu .=  '<th>Id</th>';
		$contenu .=  '<th>Pseudo</th>';
		$contenu .=  '<th>Civilité</th>';
		$contenu .=  '<th>Nom</th>';
		$contenu .=  '<th>Prénom</th>';
		$contenu .=  '<th>Mail</th>';
		
		$contenu .=  '<th>Téléphone</th>';
		$contenu .=  '<th>Statut</th>';
		$contenu .=  '<th>Del.</th>';
		$contenu .=  '<th>Modif. Statut</th>';
		$contenu .=  '<th>Modifier</th>';
		$contenu .=  '</tr>';

		// Affichage des lignes :
		while ($membre = $resultat->fetch(PDO::FETCH_ASSOC))
		{
			if ($membre['civilite'] == 'm' ) {$civilite ='Monsieur';}else{ $civilite='Madame';}
			if ($membre['statut']== '0'){$statut='Standard';}else {$statut='Admin';}
			$contenu .=  '<tr>';
			
				/* foreach ($membre as $indice => $information)
				{
					$contenu .=  '<td>' . $information . '</td>';
				} */
				$contenu .=  '<td>'.$membre['id_membre'].'</td>';
				$contenu .=  '<td>'.$membre['pseudo'].'</td>';
				$contenu .=  '<td>'.$civilite.'</td>';
				$contenu .=  '<td>'.ucfirst($membre['nom']).'</td>';
				$contenu .=  '<td>'.ucfirst($membre['prenom']).'</td>';
				$contenu .=  '<td>'.$membre['email'].'</td>';
				$contenu .=  '<td>'.$membre['telephone'].'</td>';
				$contenu .=  '<td>'.$statut.'</td>';
				$contenu .=  '<td><a href="?action=supprimer_membre&id_membre=' . $membre['id_membre'] . '" onclick="return(confirm(\'Etes-vous sûr de vouloir supprimer ce membre?\'));"><span class="glyphicon glyphicon-trash"></span></a></td>';
				$contenu .=  '<td><a href="?action=modifier_statut&id_membre=' . $membre['id_membre'] . '&statut='. $membre['statut'] .'"> modifier </a></td>';
				$contenu .=  '<td><a href="?action=afficher&id_membre=' . $membre['id_membre'].'"> <span class="glyphicon glyphicon-pencil"></span></a></td>';
			$contenu .=  '</tr>';
		}
$contenu .=  '</table>';
require_once("../inc/header.inc.php");

//var_dump($membre);
//--------------------------- Affichage ---------------------
echo $erreur;
echo $contenu;

if (isset($_GET['action'])&& $_GET['action']=="afficher"){
	$resultat = $pdo->prepare("SELECT id_membre, pseudo, nom, prenom, email, civilite, telephone, statut FROM membre WHERE id_membre=:id_membre");
	$resultat->bindValue(':id_membre',$_GET['id_membre']);
	$resultat->execute();
	$membre = $resultat->fetch(PDO::FETCH_ASSOC);
	
	
echo '<form method="post" action="?action=modifier&id_membre='.$membre['id_membre'].'" class="col-md-8 col-md-offset-2">
    <h2 class="text-center">Modifier les informations d\'un membre</h2>
  <div class="form-group">
    <label for="pseudo">Pseudo</label>
    <input type="pseudo" class="form-control" id="pseudo" name="pseudo" placeholder="Pseudo" value="'.ucfirst($membre['pseudo']).'">
  </div>
  <div class="form-group">
    <label for="nom">Nom</label>
    <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" value="'.ucfirst($membre['nom']).'">
  </div>
  <div class="form-group">
    <label for="prenom">Prénom</label>
    <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom" value="'.ucfirst($membre['prenom']).'">
  </div>
  <div class="form-group">
    <label for="email">Email</label>
    <input type="text" class="form-control" id="email" name="email" placeholder="Email"  value="'.$membre['email'].'">
  </div>
  <div class="form-group">
    <label for="civilite">Civilité</label><br>
	<select id="civilite" name="civilite" class="form-control">
		<option value="m"'; if ($membre['civilite']=='m'){ echo'selected';} echo'> Homme
		<option value="f"'; if ($membre['civilite']=='f'){ echo'selected';} echo'> Femme
	</select>
  </div>  
  <div class="form-group">
    <label for="telephone">Téléphone</label>
    <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Téléphone"  value="'.$membre['telephone'].'">
  </div>   
  <button type="submit" class="btn btn-primary col-md-12" value="">Modifier les informations du membre</button>
</form><br>

<form method="post" action="?action=modifier_mdp&id_membre='.$membre['id_membre'].'" class="col-md-8 col-md-offset-2">
    <h2 class="text-center">Modifier le mot de passe d\'un membre</h2>
  <div class="form-group">
    <label for="mdp">Mot de passe</label>
    <input type="password" class="form-control" id="mdp" name="mdp" placeholder="Insérez le mot de passe">
  </div>
  <div class="form-group">
    <label for="mdp_confirm">Confirmation du mot de passe</label>
    <input type="password" class="form-control" id="mdp_confirm" name="mdp_confirm" placeholder="Confirmez le mot de passe">
  </div>
  <button type="submit" class="btn btn-primary col-md-12" value="">Modifier le mot de passe du membre</button>
</form><br>


';

}


require_once("../inc/footer.inc.php");



