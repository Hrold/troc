<?php
require_once("inc/init.inc.php");
require_once("inc/header.inc.php");
?>
        <div class="container-fluid">
            <!--MAIN-->            
            <!-- Main jumbotron for a primary marketing message or call to action -->
            <div class="row">
                <div class="jumbotron col-md-12">
                    <div class="container">
                        <h1 class="title-univers">Troc, lier l'homme et les produits </h1>
                        <p>Au cours de ses 4 siècles d'existence, Troc n'a eu de cesse de lier les humains.</p>
						<h3  class="text-center">Fondée en 1789 - 4.3Qrd € de CA (rien que ça)</h3>
                    </div>
                </div>
            </div>
            <div class="container">
                <!-- Example row of columns -->
                <div class="row encart-univers mb-5">
                    <div class="col-md-8 offset-2 mb-3">
                        <h1 class="text-center"><span class="square square-padding">E</span>TROC</h1>
						
                    </div>
                </div>
                <div class="row histo-univers mb-3">
                    <div class="col-md-1 date-univers offset-md-2">
                        <h3>1789</h3>
                    </div>
                    <div class="col-md-9 text-univers">
                        <p>Alors que le roi est emprisonné à la Bastille par un bel après-midi ensoleillé, celui-ci est visité par le fantôme d'Obi-Wan Kenobi. Ce dernier lui souffle l'idée de créer un Troc internet où chaque membre du royaume pourrait s'échanger du bétail ainsi que le produit de ses récoltes . Comme internet n'existait pas à l'époque il fondit une société secrète qui transmettrait cette idée de génération en génération jusqu'à ce que l'univers soit prêt à sa réalisation.</p>
						<hr class="line-bottom">
                    </div>
                    
                </div>
                <div class="row histo-univers mb-3">
                    <div class="col-md-1 date-univers offset-md-2">
                        <h3>1997</h3>
                    </div>
                    <div class="col-md-9 text-univers">
                        <p>Cette année-là, la comète Halo-Biwan passe dans le ciel. Les membres de la société y voient là le signe qu'ils attendaient. Ils décident donc de créer le site et pour cela ils demandent aux élèves de l'IFOCOP de le réaliser (contre des codes promo, parce que faut pas déconner, avec les impôts qui viennent de tomber on a plus de fric...)</p>
						<hr class="line-bottom">
                    </div>
                    

                </div>
                <div class="row histo-univers mb-3">
                    <div class="col-md-1 date-univers offset-md-2">
                        <h3>1998</h3>
                    </div>
                    <div class="col-md-9 text-univers">
                        <p>8 mois plus tard et 50 retours clients après, le site sort enfin. Dès la mise en ligne c'est un énorme succès. Leur best-seller : une piscine en forme de leur maître Obi-Wan Kenobi. On se refait pas...</p>
						<hr class="line-bottom">
                    </div>
                    

                </div>
                <div class="row histo-univers mb-5">
                    <div class="col-md-1 date-univers offset-md-2">
                        <h3>2018</h3>
                    </div>
                    <div class="col-md-9 text-univers">
                        <p>20 ans ont passé et l'etroc Troc (franchement niveau orginalité on a vu mieux...) accumule les milliards de vente. Même Donald Trump a sa carte de membre. Prochain étape : créer une tartine qui ne tomberait plus du côté beurré...</p>
                    </div>
                </div>   
                   
            </div>

        </div>
       <?php  
    require_once("inc/footer.inc.php");
?>