<?php
require_once("inc/init.inc.php");
if (internauteEstConnecte()){
	$id_membre_session = $_SESSION['membre']['id_membre'];
	$resultat = $pdo->prepare("SELECT pseudo FROM membre WHERE id_membre = $id_membre_session ");
	$resultat->execute();
	$membre = $resultat->fetch(PDO::FETCH_ASSOC);
	$pseudo = $membre['pseudo'];
}
$resultat2 = $pdo->prepare('SELECT membre_id FROM annonce WHERE id_annonce = :id_annonce');
$resultat2->bindValue(':id_annonce', $_GET['id_annonce'], PDO::PARAM_INT);
$resultat2->execute();
$membre2 = $resultat2->fetch(PDO::FETCH_ASSOC);
$id_membre_annonce2 = $membre2['membre_id'];

//si on clique sur le 'deconnexion' on détruit la connexion
if(isset($_GET['action']) && $_GET['action'] == 'deconnexion')
{
    session_destroy();
}

require_once("inc/header.inc.php");

//AFFICHAGE DETAILS DE L'ANNONCE
$resultat_annonce = $pdo->prepare("SELECT a.id_annonce, a.titre, a.description_longue, a.prix, a.photo, a.adresse, a.cp, a.ville, a.membre_id, a.photo_id, a.date_enregistrement, m.nom, m.prenom, m.pseudo FROM annonce a, membre m WHERE id_annonce = :id_annonce AND a.membre_id=m.id_membre");
$resultat_annonce->bindValue(':id_annonce', $_GET['id_annonce'], PDO::PARAM_INT);
$resultat_annonce->execute();

$fiche_annonce = $resultat_annonce->fetch(PDO::FETCH_ASSOC);
//debug($fiche_annonce);
//Affichage des autres annonces du membre
$id_membre_annonce = $fiche_annonce['membre_id'];
$res_annonce_membre = $pdo->prepare("SELECT * FROM annonce WHERE membre_id = $id_membre_annonce LIMIT 0, 4");
$res_annonce_membre->execute();
$fiche_annonce_membre = $res_annonce_membre->fetchAll(PDO::FETCH_ASSOC);
//var_dump($fiche_annonce_membre);
//AJOUT COMMENTAIRE ANNONCE
    if(isset($_GET['action']) && $_GET['action'] == 'insert_comm')
    {
		if (empty($_POST['commentaire']) || strlen($_POST['commentaire']) < 10){
			$erreur .=  '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Veuillez insèrer un commentaire supérieur à 10 caractères</div>';
	} else {
        $insert_comm = $pdo->prepare("INSERT INTO commentaire (membre_id, annonce_id, commentaire, date_enregistrement)
                                VALUES (:membre_id, :id_annonce, :commentaire, now())");

        $insert_comm->bindValue(':membre_id', $id_membre_session);
        $insert_comm->bindValue(':id_annonce', $_POST['id_annonce']);
        $insert_comm->bindValue(':commentaire', $_POST['commentaire']);
        $insert_comm->execute();
		//var_dump($insert_comm);
    }
	}
//AJOUT Note
    if(isset($_GET['action']) && $_GET['action'] == 'insert_note')
    {
		$erreur2='';
		if (empty($_POST['avis']) || strlen($_POST['avis']) < 3){
			$erreur2 .=  '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Veuillez insèrer un avis supérieur à 3 caractères</div>';
		}
		if (empty($_POST['note']) || !is_int($_POST['note']) || ($_POST['note']) < 0 || ($_POST['note']) > 5) {
			$erreur2 .=  '<div class="alert alert-danger col-md-6 col-md-offset-3 text-center">Veuillez mettre une note comprise entre 0 et 5</div>';
		}
		if (empty($erreur2)){
        $insert_note = $pdo->prepare("INSERT INTO note (membre_id1, membre_id2, note, avis, date_enregistrement)
                                VALUES (:membre_id1, :membre_id2, :note, :avis, now())");

        $insert_note->bindValue(':membre_id1', $id_membre_session);
		$insert_note->bindValue(':membre_id2', $id_membre_annonce);
        $insert_note->bindValue(':note', $_POST['note']);
        $insert_note->bindValue(':avis', $_POST['avis']);
        $insert_note->execute();
		//var_dump($insert_note);
		}
    }

//AFFICHAGE COMMENTAIRES ANNONCE
$resultat_commentaire = $pdo->prepare("SELECT m.pseudo, c.commentaire, c.date_enregistrement  FROM commentaire c, annonce a, membre m WHERE a.id_annonce = :id_annonce AND c.membre_id=m.id_membre AND c.annonce_id=a.id_annonce AND a.id_annonce=:id_annonce");
$resultat_commentaire->bindValue(':id_annonce', $_GET['id_annonce'], PDO::PARAM_INT);
$resultat_commentaire->execute();

$fiche_commentaire = $resultat_commentaire->fetchAll(PDO::FETCH_ASSOC);
//debug($fiche_commentaire);
echo $erreur;
echo $erreur2;
?>

<div class="row">
    <!--INSERT TITLE ANNONCE HERE-->
    <h3 class="pull-left text-uppercase"><?= $fiche_annonce['titre'] ?></h3>
    <?php 
	echo '<button class="btn btn-primary pull-right">Contacter '.$fiche_annonce['pseudo'].'</button>'; ?>
</div>
<div class="row">
    <section>
        <div class="col-md-6 encart-photo">
            <img src="<?= $fiche_annonce['photo']?>" alt="<?=$fiche_annonce['titre']?>" class="img-responsive"></div>
        <aside class="col-md-6 encart-description">
            <span class="label-annonce">Description</span>
            <!--INSERT DESCRIPTION ANNONCE HERE-->
            <?= $fiche_annonce['description_longue'] ?>        </aside>
    </section>
</div>
<div class="row labels">
    <!--INSERT INFOS ANNONCE HERE-->
    <span class="col-md-3 glyphicon glyphicon-calendar text-center">Date de publication :
        <?php $date_init=$fiche_annonce["date_enregistrement"]; $date=date_create($date_init); echo date_format($date,"d/m/Y"); ?></span>
    <span class="col-md-3 glyphicon glyphicon-user"><?= $fiche_annonce['nom']. ' '.$fiche_annonce['prenom'] ?></span>
    <span class="col-md-2 glyphicon glyphicon-euro text-center"><?= $fiche_annonce['prix'] ?></span>
    <span class="col-md-4  glyphicon glyphicon-envelope text-center">Adresse : <span class="adresse-map"><?= $fiche_annonce['adresse']. ' '.$fiche_annonce['cp']. ' '.$fiche_annonce['ville'] ?></span></span>
</div>

<!--<div class="row">
    INSERT MAP ANNONCE HERE
	<div class="col-12" style="min-height: 200px;">
			<div id="floating-panel">
			  <input id="address" type="textbox" value="">
			  <input id="submit" type="button" value="Geocode">
			</div>
			<div id="map"></div>
			<script>

			  function initMap() {

				var map = new google.maps.Map(document.getElementById('map'), {

				  zoom: 8,

				  center: {lat: -34.397, lng: 150.644}

				});

				var geocoder = new google.maps.Geocoder();



				document.getElementById('submit').addEventListener('click', function() {

				  geocodeAddress(geocoder, map);

				});

			  }



			  function geocodeAddress(geocoder, resultsMap) {

				var address = document.getElementById('address').value;

				geocoder.geocode({'address': address}, function(results, status) {

				  if (status === 'OK') {

					resultsMap.setCenter(results[0].geometry.location);

					var marker = new google.maps.Marker({

					  map: resultsMap,

					  position: results[0].geometry.location

					});

				  } else {

					alert('Geocode was not successful for the following reason: ' + status);

				  }

				});

			  }

			</script>			
		</div>
    </div>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHvppopbRdwEQkICeXuez3MlkmDjQDUEU&callback=initMap"></script>-->


<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6f-Yp5qQ20KLsKJCkGwg-u3Dxakv2hgM&callback=initMap"> </script>-->
<div class="row">
    <!--INSERT AUTRES ANNONCE HERE-->
    <h3 class="pull-left">Autres annonces du vendeur</h3>
</div>
<div class="row">
	
	 <?php 
	 $i=0;
	 foreach($fiche_annonce_membre as $indice=>$key) {
		 $fiche_annonce_membre[$i]['photo'];
		 $i++;
	 }
    if (isset($fiche_annonce_membre[0])){ echo '<div class="col-md-2"><a href="?id_annonce='.$fiche_annonce_membre[0]['id_annonce'].'" title="'.$fiche_annonce_membre[0]['titre'].'"><img src="'.$fiche_annonce_membre[0]['photo'].'" alt="'.$fiche_annonce_membre[0]['titre'].'"  height="170"></a></div>';
	} else {
		echo '<div class="col-md-2"></div>';
	}
    if (isset($fiche_annonce_membre[0])){ echo '<div class="col-md-2 col-md-offset-1 text-center "><a href="?id_annonce='.$fiche_annonce_membre[1]['id_annonce'].'" title="'.$fiche_annonce_membre[1]['titre'].'"><img src="'.$fiche_annonce_membre[1]['photo'].'" alt="'.$fiche_annonce_membre[1]['titre'].'" height="170" class="center-block"></a></div>';
	} else {
		echo '<div class="col-md-2 col-md-offset-1"><a href="#" id="" title=""></a></div>';
	}
   if (isset($fiche_annonce_membre[2])){ echo '<div class="col-md-2 col-md-offset-1"><a href="?id_annonce='.$fiche_annonce_membre[2]['id_annonce'].'" title="'.$fiche_annonce_membre[2]['titre'].'"><img src="'.$fiche_annonce_membre[2]['photo'].'" alt="'.$fiche_annonce_membre[2]['titre'].'" height="170" class="center-block"></a></div>';
   } else {
	   echo '<div class="col-md-2 col-md-offset-1"><a href="#" id="" title=""></a></div>';
	   }
    if (isset($fiche_annonce_membre[3])){ echo '<div class="col-md-2 col-md-offset-1 pull-right"><a href="?id_annonce='.$fiche_annonce_membre[3]['id_annonce'].'" title="'.$fiche_annonce_membre[3]['titre'].'"><img src="'.$fiche_annonce_membre[3]['photo'].'" alt="'.$fiche_annonce_membre[3]['titre'].'" height="170" class="center-block"></a></div>
	</div>'; 
	}else {
	   echo '<div class="col-md-2 col-md-offset-1 pull-right"><a href="#" id="" title=""></a></div>';
	   }
	?>
</div>

<div class="row">
    <!--INSERT COMMENTAIRES HERE / GERER SI USER CONNECTE OU NON ? AIzaSyB6f-Yp5qQ20KLsKJCkGwg-u3Dxakv2hgM-->
    <h3 class="pull-left">Commentaires</h3>
</div>
<div class="row">
<?php if ($resultat_commentaire->rowcount>0)
{
    <div class="col-md-6 section-comment">
        <!--CHAQUE COMMENTAIRE EST DANS UNE DIV-->
        <?php foreach($fiche_commentaire as $indice=>$key) {
			$date_init=$fiche_commentaire[$indice]['date_enregistrement'];
			$date=date_create($date_init);
			$date_fr=date_format($date,"d/M/Y");
    echo '<div class="col-md-12">';
    echo    '<span class="label-comment glyphicon glyphicon-calendar ">'.$date_fr.'</span>';
    echo    '<span class="label-comment glyphicon glyphicon-user ">'.$fiche_commentaire[$indice]['pseudo'].'</span>';
    echo    '<span class="label-comment label-comment-content">'.$fiche_commentaire[$indice]['commentaire'].'</span>';
    echo '</div><br>';
} ?>
    </div>
	<?php
}

	if  (internauteEstConnecte()){ ?>
    <div class="col-md-5 col-md-offset-1 add-comment">
        <h4>Ajouter commentaire</h4>
        <form method="post" action="fiche_annonce.php?id_annonce=<?=$_GET['id_annonce']?>&action=insert_comm">
            <div class="form-group">
				<label for="pseudo_comment"  id="pseudo_comment" name="pseudo_comment">Votre commentaire sera visible en tant que <?php echo $pseudo;?></label>
                <input type="hidden" id="id_annonce" name="id_annonce" value="<?=$_GET['id_annonce']?>">
            </div>
            <div class="form-group">
                <textarea class="form-control" id="commentaire" name="commentaire" rows="3" placeholder="Votre commentaire ici..."></textarea>
            </div>

            <button type="submit" class="btn btn-default">Soumettre le commentaire</button>
        </form>
    </div>
	<?php } else { echo 'Vous devez être connecté afin de laisser un commentaire'; } ?>
</div>
<?php if  (internauteEstConnecte()){ ?>
<div class="row">
    <div class="col-md-6 user-review">
        <h4>Notez votre relation avec le vendeur</h4>
        <form method="post" action="fiche_annonce.php?id_annonce=<?=$_GET['id_annonce']?>&action=insert_note">
            <div class="form-group">
                <label for="note" class="col-md-4 control-label label-note">Entrez un chiffre (sur 5)</label>
                <div class="col-md-2"><input type="text" class="form-control" id="note" name="note" placeholder=""></div>
            </div>
            <div class="form-group">
                <textarea class="form-control" rows="3" id="avis" name="avis" placeholder="Insérez votre avis, ici..."></textarea>
            </div>
            <button type="submit" class="btn btn-default">Notez !</button>
        </form>
    </div>
</div>
<?php
} else { echo 'Vous devez être connecté afin de laisser un laisser une note au vendeur'; } 
    require_once("inc/footer.inc.php");


