<?php
if ( isset( $_GET['q'] ) && $_GET['q'] !== '' )
{
  require( '../includes/pdo_connect.php' );


  $mon_tableau = array('Harold', 'Fatna', 'Vincent');
  $mon_tableau[] = 'Hatem';
  $mon_tableau[] = 'Thomas';



  $sql = 'SELECT mot FROM mots WHERE mot LIKE :q';
  $stmt = $pdo->prepare($sql);
  $stmt->bindValue(':q', $_GET['q'] . '%', PDO::PARAM_STR);

  $tableau_mots = array(); // Création d'un tableau vide qui contiendra les mots trouvés

  if ($stmt->execute()) {
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $tableau_mots[] = $row['mot'];
    }
  }

  echo json_encode( $tableau_mots ); // retourne un tableau simple (plus léger)
}
?>
