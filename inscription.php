<?php
require_once("inc/init.inc.php");
require_once("inc/header.inc.php");
/*
    1. Réaliser un formulaire d'inscription correspondant à la table membre (sauf id_membre, statut) 
    2. Contrôler que l'on réceptionne bien toute les données saisies du formulaire en PHP
    3. Contrôler les champs suivants : 
     - contrôler la disponibilité du pseudo et de l'email
     - contrôler la taille des champs , pseudo, mdp : entre 2 et 20 caractères
     - contrôler le champs code postal: integer et taille de 5 caractères
*/
if(internauteEstConnecte()) // si l'internaute est connecté, il n'a rien à faire sur la page connexion, on le redirige vers sa page profil
{
    header("location:profil.php");
}
if($_POST)
{
    $erreur = '';
    
    $verif_pseudo = $pdo->prepare("SELECT * FROM membre WHERE pseudo = :pseudo");
    $verif_pseudo->bindValue(':pseudo', $_POST['pseudo'], PDO::PARAM_STR);
    $verif_pseudo->execute();
    if($verif_pseudo->rowCount() > 0)
    {
        $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Pseudo indisponible!!</div>';
    }
    //------------------------------------------------------
    $verif_email = $pdo->prepare("SELECT * FROM membre WHERE email = :email");
    $verif_email->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
    $verif_email->execute();
    if($verif_email->rowCount() > 0)
    {
         $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Email existant!!</div>';
    }
    //------------------------------------------------------
    if(strlen($_POST['pseudo']) < 2 || strlen($_POST['pseudo']) > 20)
    {
         $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille de pseudo non valide : entre 4 et 20 caractères!</div>';
    }
    //------------------------------------------------------
    if(strlen($_POST['mdp']) < 8 || strlen($_POST['mdp']) > 20)
    {
         $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille de mdp non valide : entre 8 et 20 caractères!</div>';
    }
    //------------------------------------------------------
    if(!is_numeric($_POST['telephone']) || strlen($_POST['telephone']) !== 10)
    {
         $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Taille ou format téléphone non valide !</div>';
    }
    //------------------------------------------------------
    // contrôler que le mdp soit confirmé        
    if($_POST['mdp'] !== $_POST['mdp_confirm'])
    {
        $erreur .= '<div class="alert alert-danger col-md-8 col-md-offset-2 text-center">Vérifier les mots de passe</div>';
    }
    //------------------------------------------------------
    if(empty($erreur)) // si la variable $erreur est vide, c'est que nous ne sommes pas entreé dans les condotions IF, l'internaute a bien rempli le formulaire, nous pouvons donc executer l'insertion
    {
        $mdp = password_hash($_POST['mdp'], PASSWORD_DEFAULT); // password_hash() permet de créer un clé de hachage, on ne garde jamais en clair les mot de passe dans la BDD
        
        // requete d'insertion
        $resultat = $pdo->prepare("INSERT INTO membre (pseudo, mdp, nom, prenom, email, civilite, telephone, date_enregistrement)VALUES (:pseudo, :mdp, :nom, :prenom, :email, :civilite, :telephone, NOW())");
        
        $resultat->bindValue(':pseudo', $_POST['pseudo'], PDO::PARAM_STR);
        $resultat->bindValue(':mdp', $mdp);
        $resultat->bindValue(':nom', $_POST['nom'], PDO::PARAM_STR);
        $resultat->bindValue(':prenom', $_POST['prenom'], PDO::PARAM_STR);
        $resultat->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $resultat->bindValue(':civilite', $_POST['civilite'], PDO::PARAM_STR);
        $resultat->bindValue(':telephone', $_POST['telephone'], PDO::PARAM_STR);
		//$resultat->bindValue(':date_enregistrement', NOW());
        
        $resultat->execute();
        var_dump($resultat);
        $content .= '<div class="alert alert-success col-md-8 col-md-offset-2 text-center">Vous êtes inscrit sur notre site. <a href="connexion.php" class="alert-link">Cliquez ici pour vous connecter</a></div>';    
    }
    
    $content .= $erreur; 

    echo $content;    
}

?>

<form method="post" action="" class="col-md-8 col-md-offset-2">
    <h2 class="text-center">INSCRIPTION</h2>
  <div class="form-group">
    <label for="pseudo">Pseudo</label>
    <input type="pseudo" class="form-control" id="pseudo" name="pseudo" placeholder="Pseudo">
  </div>
  <div class="form-group">
    <label for="mdp">Mot de passe</label>
    <input type="password" class="form-control" id="mdp" name="mdp" placeholder="Insérer le mot de passe">
  </div>
  <div class="form-group">
    <label for="mdp_confirm">Confirmer mot de passe</label>
    <input type="password" class="form-control" id="mdp_confirm" name="mdp_confirm" placeholder="Confirmer le mot de passe">
  </div>
  <div class="form-group">
    <label for="nom">Nom</label>
    <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom">
  </div>
  <div class="form-group">
    <label for="prenom">Prénom</label>
    <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom">
  </div>
  <div class="form-group">
    <label for="email">Email</label>
    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="civilite">Civilité</label><br>
    <input type="radio" name="civilite" id="civilite" value="m" checked> Homme
    <input type="radio" name="civilite" id="civilite" value="f"> Femme
  </div>  
  <div class="form-group">
    <label for="telephone">Téléphone</label>
    <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Téléphone">
  </div>    
  <button type="submit" class="btn btn-primary col-md-12">Inscription</button>
</form>

<?php
require_once("inc/footer.inc.php");

