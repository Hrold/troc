<?php
require_once("inc/init.inc.php"); 
require_once("inc/header.inc.php");
?>
<div class="row row-offcanvas row-offcanvas-right">
<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
          <div class="list-group">
		  </div>
        </div><!--/.sidebar-offcanvas-->

        <div class="col-xs-12 col-sm-9 col-lg-12 ">
   
          <div class="jumbotron">
            <h1>Bienvenue dans la boutique Troc </h1>
            <p>Aussi bien pour échanger des services, ou acheter des produits, vous trouverez tout cela sur Troc.C'est bien simple, sur les 15 derniers jours, nous faisons mieux que le Bon coin. Quoique vous vendiez ou cherchiez, vous pouvez le faire sur Troc.</p>
          </div>
		  
		  <?php
			//if(isset($_GET['categorie'])):
			
			$donnees = $pdo->prepare("SELECT * FROM annonce");
			$donnees->execute();
			while($annonce = $donnees->fetch(PDO::FETCH_ASSOC)):
				$id_membre = $annonce['membre_id'];
				$donnees2 = executeRequete("SELECT pseudo FROM membre WHERE id_membre = $id_membre");
				$membre = $donnees2->fetch(PDO::FETCH_ASSOC);
				
				//debug($nb_notes);
				
				
			//debug($annonce);
		  ?>
		  
          <!--<div class="row">-->
            <div class="col-xs-12 col-lg-4" style="margin-bottom:20px;">
			<div class="panel-default border">
			<div class="panel-heading text-center" style="min-height:440px"><h2><?=$annonce['titre'] ?></h2>
              <p><a href="<?=$annonce['photo']?>" class="html5lightbox"><img src="<?=$annonce['photo']?>" alt="<?=$annonce['titre'] ?>" class="img-responsive"  width="225" height="225"></a></p>
			  
			  <p class="text-center">Prix : <?= $annonce['prix'] ?> €</p>
			  <p class="text-center">Courte description : <?= substr($annonce['description_courte'],1,100) ?></p>
			  <p class="text-center">Mis en vente par : <?= $membre['pseudo'] ?></p>
			  <p class="text-center"><?php  
			  $donnees3 = executeRequete("SELECT note FROM note WHERE membre_id2 = $id_membre");
				$nb_notes = $donnees3->rowCount();
				if ($nb_notes != 0) {
					$noteTotal = 0;
					while ($membre2 = $donnees3->fetch(PDO::FETCH_ASSOC)){
						$noteTotal += $membre2['note'];
					}
					$note = $noteTotal/$nb_notes;
					//number_format
					echo 'Moyenne des notes : '.number_format($note,2);
				} else {
					echo 'Ce membre n\'a pas encore obtenu de note';
				}
			

			  ?></p>
              <p><a class="btn btn-default" href="fiche_annonce.php?id_annonce=<?=$annonce['id_annonce']?>" role="button">Voir &raquo;</a></p>
            </div><!--/.col-xs-6.col-lg-4-->
          </div>
		 </div>
	

<?php
endwhile;

?>
        </div><!--/.col-xs-12.col-sm-9-->
</div>
        
      <!--/row-->
<?php


require_once("inc/footer.inc.php");